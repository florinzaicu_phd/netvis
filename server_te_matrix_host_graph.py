#!/user/bin/python
import os
from te_matrix_host_graph import generate_page
from base_server import BaseServer


class Graph(BaseServer):
    TITLE = "TE Matrix Host Graph"
    DATA_FILES = {"Default": ["te_matrix.txt", "hosts.txt"]}

    def load_data(self, name):
        data_dir = os.path.abspath(os.path.join("DATA", name))
        te_matrix_input = os.path.join(data_dir, "te_matrix.txt")
        host_input = os.path.join(data_dir, "hosts.txt")
        layout = generate_page(te_matrix_input, host_input)
        self.update_page_content(layout)


# Generate the page
Graph()
