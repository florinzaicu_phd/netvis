#!/bin/bash

declare -a SWLIST=("swctrlV1" "swctrlV2" "swctrlV3" "swctrlV4" "swctrlV5" "swctrlV6" "swctrlV7" "swctrlV8" "swctrlV9")

for swl in "${SWLIST[@]}";
do
    echo $swl;
    ./merge.py --files DATA/TMP_minover_"$swl"/MeanCongestionVsIterations.dat DATA/TMP_minover_"$swl"_TEST/MeanCongestionVsIterations.dat DATA/TMP_minover_"$swl"_TEST2/MeanCongestionVsIterations.dat --out DATA/"$swl"/MeanCongestionVsIterations.dat --filter minoverlapmctrl
    ./merge.py --files DATA/TMP_minover_"$swl"/MaxCongestionVsIterations.dat DATA/TMP_minover_"$swl"_TEST/MaxCongestionVsIterations.dat DATA/TMP_minover_"$swl"_TEST2/MaxCongestionVsIterations.dat --out DATA/"$swl"/MaxCongestionVsIterations.dat --filter minoverlapmctrl
    ./merge.py --files DATA/TMP_minover_"$swl"/MeanExpCongestionVsIterations.dat DATA/TMP_minover_"$swl"_TEST/MeanExpCongestionVsIterations.dat DATA/TMP_minover_"$swl"_TEST2/MeanExpCongestionVsIterations.dat --out DATA/"$swl"/MeanExpCongestionVsIterations.dat --filter minoverlapmctrl
    ./merge.py --files DATA/TMP_minover_"$swl"/MaxExpCongestionVsIterations.dat DATA/TMP_minover_"$swl"_TEST/MaxExpCongestionVsIterations.dat DATA/TMP_minover_"$swl"_TEST2/MaxExpCongestionVsIterations.dat --out DATA/"$swl"/MaxExpCongestionVsIterations.dat --filter minoverlapmctrl
    ./merge.py --files DATA/TMP_minover_"$swl"/CongestionLossVsIterations.dat DATA/TMP_minover_"$swl"_TEST/CongestionLossVsIterations.dat DATA/TMP_minover_"$swl"_TEST2/CongestionLossVsIterations.dat --out DATA/"$swl"/CongestionLossVsIterations.dat --filter minoverlapmctrl
    ./merge.py --files DATA/TMP_minover_"$swl"/MinoverlapOptiTECountVsIterations.dat DATA/TMP_minover_"$swl"_TEST/MinoverlapOptiTECountVsIterations.dat DATA/TMP_minover_"$swl"_TEST2/MinoverlapOptiTECountVsIterations.dat --out DATA/"$swl"/MinoverlapOptiTECountVsIterations.dat --filter minoverlapmctrl

    ./merge.py --files DATA/TMP_minover_"$swl"/EdgeCongestionVsIterations.dat DATA/TMP_minover_"$swl"_TEST/EdgeCongestionVsIterations.dat DATA/TMP_minover_"$swl"_TEST2/EdgeCongestionVsIterations.dat --out DATA/"$swl"/EdgeCongestionVsIterations.dat --multi_line --filter minoverlapmctrl
    ./merge.py --files DATA/TMP_minover_"$swl"/EdgeExpCongestionVsIterations.dat DATA/TMP_minover_"$swl"_TEST/EdgeExpCongestionVsIterations.dat DATA/TMP_minover_"$swl"_TEST2/EdgeExpCongestionVsIterations.dat --out DATA/"$swl"/EdgeExpCongestionVsIterations.dat --multi_line --filter minoverlapmctrl
    ./merge.py --files DATA/TMP_minover_"$swl"/LatencyDistributionVsIterations.dat DATA/TMP_minover_"$swl"_TEST/LatencyDistributionVsIterations.dat DATA/TMP_minover_"$swl"_TEST2/LatencyDistributionVsIterations.dat --out DATA/"$swl"/LatencyDistributionVsIterations.dat --multi_line --filter minoverlapmctrl
    ./merge.py --files DATA/TMP_minover_"$swl"/MinoverlapMctrlTEOptiCountVsIterations.dat DATA/TMP_minover_"$swl"_TEST/MinoverlapMctrlTEOptiCountVsIterations.dat DATA/TMP_minover_"$swl"_TEST2/MinoverlapMctrlTEOptiCountVsIterations.dat --out DATA/"$swl"/MinoverlapMctrlTEOptiCountVsIterations.dat --multi_line --filter minoverlapmctrl

    #./merge_paths_folder.py --folders DATA/TMP_minover_"$swl"/paths DATA/TMP_minover_"$swl"_TEST/paths DATA/TMP_minover_"$swl"_TEST2/paths --out DATA/"$swl"/paths --filter minoverlapmctrl
    #python path_edges_used.py --topo DATA/"$swl"/topo.gml --node_assoc DATA/"$swl"/node_assoc.json --sw_ctrl_map DATA/"$swl"/sw_ctrl_map.json --data DATA/"$swl"/ --compile
done

