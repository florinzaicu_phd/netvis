#!/user/bin/python
import sys
import os
import re
import json
import math
from argparse import ArgumentParser

# Visualisation Libraries
from bokeh.io import show, output_file
from bokeh import models as bkm
from bokeh import layouts as bkl
from bokeh import palettes as bkp

# Import helper methods
from graph_helper import init_nx, init_plot, gen_plot_style_controls


# --------------------------------------------------------
# Graph that simply displays a topology
# --------------------------------------------------------


# JS code that is used to show the node and host information on selection of a node
JS_node_select = """var node_data = n_ds.data;
var ind = n_ds.selected["1d"]["indices"][0];
var i_ds = i_ds.data;
div = document.getElementById("embed");

if (ind == null) {
    div.innerHTML = "Click on node to view edge info!";
    return;
}

var node_country = node_data["Country"][ind];
var node_label = node_data["label"][ind];
var node_host = "N/A";
if ("Host" in node_data)
    node_host = node_data["Host"][ind];

div.innerHTML = "<strong>Selected Node Info</strong><br />";
div.innerHTML += "<em>ID:</em> "+ind+"<br />";
div.innerHTML += "<em>Label:</em> "+node_label+"<br />";
div.innerHTML += "<em>Country:</em> "+node_country+"<br />";
div.innerHTML += "<em>Host:</em> "+node_host+"<br />";

div.innerHTML += "<br /><strong>Selected Node Edges Info</strong><br />";

for (var i = 0; i < i_ds["from"].length; i++) {
    if (i_ds["from"][i] == node_data["index"][ind]) {
        div.innerHTML += "<hr /><strong>Link Info</strong><br />";
        div.innerHTML += "<em>ID:</em> "+i+"<br />";
        div.innerHTML += "<em>From:</em> "+i_ds["from"][i]+"<br />";
        div.innerHTML += "<em>To:</em> "+i_ds["to"][i]+"<br />";
    }
}
"""

# JS code to change if we are showing domains on topology
JS_select_display_dom = """var val = cb_obj.value;

for (var i = 0; i < e_ds.data["start"].length; i++) {
    e_ds.data["color"][i] = id_info["edge_default"]["color"];
}
for (var i = 0; i < n_ds.data["id"].length; i++) {
    n_ds.data["color"][i] = id_info["node_default"]["color"];
}

if (val == "Yes") {
    for (var i = 0; i < e_ds.data["start"].length; i++) {
        test_idp = e_ds.data["start"][i] + "-" + e_ds.data["end"][i];
        if (id_info["link"].includes(test_idp)) {
            e_ds.data["color"][i] = "#ff0000";
        }
    }

    for (var i = 0; i < n_ds.data["id"].length; i++) {
        test_node = n_ds.data["index"][i];
        if (test_node in id_info["node"]) {
            n_ds.data["color"][i] = id_info["node"][test_node];
        }
    }
}

n_ds.change.emit();
e_ds.change.emit();
"""

def generate_page(topo_input, node_assoc=None, sw_ctrl_map=None, host_name=None):
    """ Generate and return page layout that shows a visual representation of a topology
    `topo_input`.

    Args:
        topo_input (str): Path to the topology file used to collect results
        node_assoc (str): Optional path to JSON file that associations result
            node name to topo node ID.
        sw_ctrl_map (str): Optional switch to controller map file to show
            domains on topology
        host_name (str): Optional path to JSON file that describes host
            name of labels (prevents auto generation with node IDs).

    Returns:
        obj: Page layout to display
    """
    # Load the host name data from the provided file
    host_names  = None
    if host_name is not None:
        with open(host_name, "r") as fin:
            host_names = json.loads(fin.read())

    # Load the topology data and initiate the info DS
    info_ds = {
        "from": [],
        "to": [],
        "xs": [],
        "ys": []
    }
    g, pos, info_ds = init_nx(topo_input, info_ds, host_names=host_names)

    # Load the association data from the provided file
    assoc_data = {}
    if node_assoc is not None:
        with open(node_assoc, "r") as fin:
            assoc_data = json.loads(fin.read())

    # Generate the plot and renderers
    plot, graph_ren = init_plot("Display Topology", g, pos)
    node_ren = graph_ren.node_renderer
    edge_ren = graph_ren.edge_renderer
    node_ds = node_ren.data_source
    edge_ds = edge_ren.data_source

    callback_node = bkm.CustomJS(args=dict(n_ds=node_ds, i_ds=info_ds), code=JS_node_select)
    node_ds.selected.js_on_change('indices', callback_node)

    # Go through and process the idp links information
    inter_dom_info = {"link": [], "node": {},
        "edge_default": {"color": "#cccccc", "sel_color": bkp.Spectral4[2]},
        "node_default": {"color": bkp.Spectral4[0], "sel_color": bkp.Spectral4[2]}
    }
    domains = []
    if sw_ctrl_map is not None:
        with open(sw_ctrl_map, "r") as fin:
            tmp = json.loads(fin.read())
            for ctrl,ctrl_d in tmp["ctrl"].items():
                # Process the switches of the domain
                for sw in ctrl_d["sw"]:
                    if ctrl not in domains:
                        domains.append(ctrl)
                    i = domains.index(ctrl)
                    if sw in assoc_data:
                        sw = assoc_data[sw]
                    inter_dom_info["node"][sw] = bkp.Pastel1[9][i]

                # Process the inter-domain links
                for dom,dom_d in ctrl_d["dom"].items():
                    for info in dom_d:
                        info_from = info["sw"]
                        info_to = info["sw_to"]
                        if info_from in assoc_data:
                            info_from = assoc_data[info_from]
                        if info_to in assoc_data:
                            info_to = assoc_data[info_to]

                        idp = "%s-%s" % (info_from, info_to)
                        if idp not in inter_dom_info["link"]:
                            inter_dom_info["link"].append(idp)

    # Create container to show info of clicked nodes and connected links
    embed_div = bkm.Div(text="<div id='embed'>Click on node to view edge info!</div>",
        style={"padding": "20px", "width": "80%", "background": "#eee", "box-sizing":
                "border-box"})

    # Create dd to show or hide multi-controller domains
    dom_dd = bkm.Select(title="Show Domains:", value="No", options=["No", "Yes"], height=50,
                            sizing_mode="stretch_width")
    dom_dd.js_on_change("value", bkm.CustomJS(args=dict(n_ds=node_ds, e_ds=edge_ds,
                            id_info=inter_dom_info), code=JS_select_display_dom))

    # Configure the output file attributes, built the page layout and show the graph
    ui_ctrls = gen_plot_style_controls(plot)
    if sw_ctrl_map is not None:
        ui_ctrls.append(dom_dd)

    opt_layout = bkl.column(children=ui_ctrls, width=120)
    main_layout = bkl.row(children=[opt_layout, bkl.Spacer(width=10), plot])
    page_layout = bkl.column(children=[main_layout, embed_div],
                                sizing_mode="scale_both", margin=(5,20,5,5))
    return page_layout

if __name__ == "__main__":
    # Initiate the argument parser and validated arguments
    parser = ArgumentParser("Display Topo Graph")
    parser.add_argument("--topo", required=True, type=str,
            help="Topology file (.dot or .gml)")
    parser.add_argument("--node_assoc", required=False, type=str, default=None,
            help="Node association JSON file (optional)")
    parser.add_argument("--sw_ctrl_map", required=False, type=str, default=None,
            help="Minoverlap switch to controller JSON map file (optional)")
    parser.add_argument("--host_name", required=False, type=str, default=None,
            help="JSON File that specifies host names for nodes (optional)")
    args = parser.parse_args()

    if not os.path.exists(args.topo) or not os.path.isfile(args.topo):
        print("Please enter a valid topology file")
        exit(1)

    # If the node assoc file is invalid do not load it
    if (args.node_assoc is None or not os.path.exists(args.node_assoc)
            or not os.path.isfile(args.node_assoc)):
        args.node_assoc = None

    # If the switch to controller map argument is invalid do not load it
    if (args.sw_ctrl_map is None or not os.path.exists(args.sw_ctrl_map)
            or not os.path.isfile(args.sw_ctrl_map)):
        args.sw_ctrl_map = None

    # If the host name file argument is invalid do not load it
    if (args.host_name is None or not os.path.exists(args.host_name)
            or not os.path.isfile(args.host_name)):
        args.host_name = None

    output_file("display_topo_graph.html", title="Display Topology Graph")
    show(generate_page(args.topo, args.node_assoc, args.sw_ctrl_map, args.host_name))
