#!/usr/bin/python

# -------------------------------------------------------------
#
# Script that merges multiple yates data results files into one
# to be used for plotting. Method takes in two or more files which
# it combines into a single file. The first file is copied unmodified
# to the output file while the subsequent files have their algorithm
# name modified by appending the index of the file (to prevent conflicts).
# Note that any filtering only applies to subsequent files (not the first
# file).
#
# Usage:
#   ./merge.py --files <files ...> --out <out> --filter [filter]
#
#   <files ...> = List of files to merge (two or more)
#
#   <out> = Final merged file path
#
#   [filter] = List of strings that specify the algorithms to include
#              in the merged file. Note that this option dosen't apply
#              to the first file as this is simply copied over.
#
# -------------------------------------------------------------

from argparse import ArgumentParser
import os
import sys
import re


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--files", required=True, type=str, nargs="+",
        help="List of file paths to merge")
    parser.add_argument("--out", required=True, type=str, help="Output merge file path")
    parser.add_argument("--filter", required=False, type=str, default=None, nargs="+",
        help="Only merge these algorithms to the first file (first file not filtered)")
    parser.add_argument("--multi_line", required=False, action="store_true",
        help="Data is multi-line (algorithm and iteration line followed by data lines)")

    # Parse the arguments and make sure the input file is valid and exists
    args = parser.parse_args()
    if len(args.files) < 2:
        print("Please enter at-lest two files to merge")
        sys.exit(1)

    for fpath in args.files:
        if not os.path.exists(fpath) or not os.path.isfile(fpath):
            print("Invalid file path (%s)" % fpath)
            sys.exit(1)

    print("Starting to merge files ...")
    with open("merge.temp", "w") as output:
        for index in range(len(args.files)):
            print("\tMerging file %s" % args.files[index])
            if index > 1:
                output.write("\n")
            with open(args.files[index], "r") as input:
                for line in input:
                    if index == 0:
                        # If this is the first file just copy its data
                        output.write(line)
                    else:
                        # Skip over any comments or empty lines
                        if line.startswith("#") or line == "\n":
                            continue

                        # Process a single lined data file
                        if not args.multi_line:
                            # Get the series name and see if it applies to filter
                            split = line.split("\t")
                            series = split[0]
                            if args.filter is not None and series not in args.filter:
                                continue

                            # Add the line to the output file
                            split[0] = split[0] + "_" + str(index)
                            output.write("\t".join(split))

                        # Otherwise process a multi lined data file
                        else:
                            # Find out if the line is data or series name
                            if ":" in line:
                                output.write(line)
                            else:
                                reg = re.search("([a-zA-Z0-9_]+)\s([0-9]+)", line)
                                if reg is None:
                                    print("Can't parse no data line %s, skipping" % line)
                                    continue

                                algo = reg.group(1)
                                iter = reg.group(2)
                                algo = "%s_%d" % (algo, index)
                                output.write("%s\t%s\n" % (algo, iter))


    # XXX: Use a temp file to prevent issues when reading and writing to same file
    os.rename("merge.temp", args.out)
    print("Done, merged files to output %s" % args.out)
