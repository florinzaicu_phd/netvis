#!/user/bin/python
import sys
import os
import re
import math
from argparse import ArgumentParser

# Visualisation Libraries
from bokeh.io import show, output_file
from bokeh import models as bkm
from bokeh import layouts as bkl
from bokeh import palettes as bkp
from bokeh.plotting import figure


# --------------------------------------------------------
# Graph that shows TE matrix total traffic as a line graph
# --------------------------------------------------------


JS_SCALE_CHANGE = """var scale_txt = cb_obj.value;
var scale = parseFloat(scale_txt);
if (isNaN(scale)) {
    alert("Please enter a valid numeric scale factor!");
    return;
}
if (scale < 0.1) {
    alert("Scale factor can't be less than 0.1");
    return;
}

// Compute the scale modification required based on the old scale value
var scale_mod = scale / opt_ds.data["scale"][0];
opt_ds.data["scale"][0] = scale;

// Update the data to match the new scale
for (var i = 0; i < data_ds.data["y"].length; i++) {
    var old_data = data_ds.data["y"][i];
    data_ds.data["y"][i] = old_data * scale_mod;
}

// Change the title and emit DS change
var old_title = plot.title.text.split("(")[0];
plot.title.text = old_title + "(Scale: " + scale + ")";
data_ds.change.emit();
"""

JS_CHANGE_UNITS = """var units_txt = cb_obj.value;
var units = 1;
if (units_txt == "kbps")
    units = 1000;
else if (units_txt == "mbps")
    units = 1000 * 1000;
else if (units_txt == "gbps")
    units = 1000 * 1000 * 1000;

// Get the old unit text to work out required DS modification
var old_units_txt = opt_ds.data["units"][0];
var old_units = 1;
if (old_units_txt == "kbps")
    old_units = 1000;
else if (old_units_txt == "mbps")
    old_units = 1000 * 1000;
else if (old_units_txt == "gbps")
    old_units = 1000 * 1000 * 1000;

var units_mod = units / old_units;
opt_ds.data["units"][0] = units_txt;

// Update the datasource to show new units
for (var i = 0; i < data_ds.data["y"].length; i++) {
    var old_data = data_ds.data["y"][i];
    data_ds.data["y"][i] = old_data / units_mod;
    data_ds.data["units"][i] = units_txt;
}

// Update y axis label and emit DS change
plot.left[0].axis_label = "Traffic (" + units_txt + ")";
data_ds.change.emit();
"""

JS_TYPE_CHANGE = """var type = cb_obj.value;
// Process the display units
var units = 1;
if (opt_ds.data["units"][0] == "kbps")
    units = 1000;
else if (opt_ds.data["units"][0] == "mbps")
    units = 1000 * 1000;
else if (opt_ds.data["units"][0] == "gbps")
    units = 1000 * 1000 * 1000;

// Retrieve the scale factor
var scale = opt_ds.data["scale"][0];

// Process target type
var target_key = "";
if (type == "Total") {
    target_key = "total";
    plot.title.text = "TE Matrix Traffic - Total";
} else if (type == "Average") {
    target_key = "avg";
    plot.title.text = "TE Matrix Traffic - Average";
}

// Add scale factor to title
plot.title.text += " (Scale: " + scale + ")";

// Update data based on selection
for (var i = 0; i < data_ds.data["y"].length; i++) {
    var value = (stats_data[target_key][i] * scale) / units;
    data_ds.data["y"][i] = value;
}

// Emit DS change
data_ds.change.emit();
"""

def generate_page(te_matrix_input, host_input):
    """ Generate and return the page layout showing the data from `te_matrix_input`. TE
    matrix total traffic is shown by summing all host rows in the TE matrix lines and
    displaying a line graph for every iteration. Hosts are provided by the host file
    `host_input`. This file is only used to work out the NxM dimmension of the matrix.

    Returns:
        obj: Page layout to display.
    """
    stats_data = {}
    max_iteration = 0
    hosts = []

    # Read the hosts input and get the list of hosts
    with open(host_input, "r") as fin:
        for line in fin:
            line = line.strip()

            # Skip empty lines
            if line ==  "":
                continue

            hosts.append(line)

    # Prime the total and average part of the data array
    stats_data["total"] = []
    stats_data["avg"] = []

    # Rread the TE matrix file
    with open(te_matrix_input, "r") as fin:
        for line in fin:
            line = line.strip()

            # Skip empty lines
            if line == "":
                continue

            # Tokenize the rows and process
            tokens = line.split(" ")
            total = 0.0
            for i in range(len(tokens)):
                # Compute the from and to index based on the token index and skip if host send
                # to itself
                hfrom_index = math.floor(i / len(hosts))
                hto_index = i % len(hosts)
                if hfrom_index == hto_index:
                    continue

                val = float(tokens[i])
                total += val

            # Set the data values from the aggregated column data
            stats_data["total"].append(total)
            stats_data["avg"].append(total / len(hosts))
            max_iteration += 1

    # Initiate the graph and data-sources
    iters = list(range(1, max_iteration + 1))
    data_ds = bkm.ColumnDataSource(dict(x=iters, y=stats_data["total"], units=(["bps"] * max_iteration)))

    plot = figure(tools="", toolbar_location="right", sizing_mode="stretch_both")
    plot.line(x="x", y="y", source=data_ds, line_width=2)
    plot.circle(x="x", y="y", source=data_ds, size=5)

    plot.xaxis.axis_label = "Iteration \#"
    plot.yaxis.axis_label = "Traffic (bps)"

    # Configure the title and legend location
    plot.title.text = "TE Matrix Traffic - Total (Scale: 1)"

    # Initiate the tools for the graph
    hover_tl = bkm.HoverTool(show_arrow=False, tooltips=[("Iteration", "@x"), ("Traffic", "@y"),
                                                            ("Unit", "@units")])
    zoom_tl = bkm.WheelZoomTool()
    plot.toolbar.active_scroll = zoom_tl
    plot.add_tools(hover_tl, bkm.TapTool(), zoom_tl, bkm.PanTool(), bkm.ResetTool(), bkm.SaveTool())

    # Datasource that stores options like scale and units
    opt_ds = bkm.ColumnDataSource(dict(scale=[1.0], units=["bps"]))

    # Initiate the controls

    # Scale text input
    scale_tx = bkm.TextInput(title="Scale Factor:", value="1.0")
    scale_change_callback = bkm.CustomJS(args=dict(opt_ds=opt_ds, data_ds=data_ds, plot=plot),
                                code=JS_SCALE_CHANGE)
    scale_tx.js_on_change("value", scale_change_callback)

    # Unit drop down
    unit_dd = bkm.Select(title="Units:", value="bps", options=["bps", "kbps", "mbps", "gbps"], height=50,
                        sizing_mode="stretch_width")
    unit_change_callback = bkm.CustomJS(args=dict(opt_ds=opt_ds, data_ds=data_ds, plot=plot),
                                            code=JS_CHANGE_UNITS)
    unit_dd.js_on_change("value", unit_change_callback)

    # Type drop down
    type_dd = bkm.Select(title="Display:", value="Total", options=["Total", "Average"],
                            height=50, sizing_mode="stretch_width")
    type_change_callback = bkm.CustomJS(args=dict(opt_ds=opt_ds, stats_data=stats_data, data_ds=data_ds,
                                    plot=plot), code=JS_TYPE_CHANGE)
    type_dd.js_on_change("value", type_change_callback)


    # Configure the output file attributes, built the page layout and show the graph
    row_dd = bkl.row(children=[scale_tx, unit_dd, type_dd], sizing_mode="stretch_width")
    page_layout = bkl.column(children=[plot, row_dd], sizing_mode="scale_both",
                                margin=(5,20,5,5))
    return page_layout

if __name__ == "__main__":
    # Initiate the argument parser and validate arguments
    parser = ArgumentParser("TE Matrix Graph")
    parser.add_argument("--data", required=True, type=str, help="YATES TE matrix file")
    parser.add_argument("--host", required=True, type=str, help="YATES host file")
    args = parser.parse_args()

    if not os.path.exists(args.data) or not os.path.isfile(args.data):
        print("Please enter a valid TE Matrix file")
        exit(1)

    if not os.path.exists(args.host) or not os.path.isfile(args.host):
        print("Please enter a valid host mapping file")
        exit(1)

    output_file("te_matrix_graph.html", title="TE Matrix Traffic Graph")
    show(generate_page(args.data, args.host))
