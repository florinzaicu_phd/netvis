#!/user/bin/python
import os
from helixMC_te_opti_req_graph import generate_page
from base_server import BaseServer


class Graph(BaseServer):
    TITLE = "HelixMC TE Optimisation Request Graph"
    DATA_FILES = {"Default": ["HelixMCTEOptiCountVsIterations.dat"]}

    def load_data(self, name):
        data_dir = os.path.abspath(os.path.join("DATA", name))
        input = os.path.join(data_dir, "HelixMCTEOptiCountVsIterations.dat")
        layout = generate_page(input)
        self.update_page_content(layout)


# Generate the page
Graph()
