#!/user/bin/python
import os
from bokeh import models as bkm
from bokeh import layouts as bkl
from bokeh.plotting import curdoc


class BaseServer():
    """ Base Bokeh server class that contains usefull methods to allow dataset changes and
    also interaction. All graphs to be used in server mode should inherit from this class.

    NOTE: Inheriting graphs should over-ride `:attr:class:(DATA_FILES)` to contain the list
    of data files that are required for the graph.

    Attributes:
        DATA_FILES (dict): Datatype list of files requird to validate dataset folder
        DEFAULT_DATA_TYPE (str): Default datatype to load when page starts
        TITLE (str): Base title of the page

        data_type (str): Current data type shown on UI
        dataset (str): Current selected dataset name on UI
        dataset_names (str): List of avaible dataset names for the datatype
        __dataset_dd (obj): Dataset drop down instance shown on page
        __root (obj): Main page content column instance
    """

    # BASE TITLE OF THE PAGE
    TITLE = "..."

    # DICTIONARY OF REQUIRED FILES IN THE DATASET FOLDERS WHERE EACH KEY OF THE DICTIONARY
    # REPRESENTS A DIFFERENT DATA TYPE TO DISPLAY.
    #
    # XXX: A string data value represents a single file, a tuple of strings multiple files where
    # any of the items needs to exist and a dictionary contains a file or directory name
    # with a value specifying the type expected, i.e. ("file" or "folder" respectively).
    DATA_FILES = {}

    # DEFAULT DATA TYPE TO LOAD (IF DIFFERENT OVERRIDE IN CHILD)
    DEFAULT_DATA_TYPE = "Default"

    def __init__(self):
        self.data_type = self.DEFAULT_DATA_TYPE
        self.dataset = ""
        self.dataset_names = []
        self.__dataset_dd = None
        self.__root = bkm.Column(sizing_mode="scale_both", margin=(5,20,5,5))

        # Initiate the page attributes and add the root container
        curdoc().title = self.TITLE
        curdoc().add_root(self.__root)

        # Load the avaible dataset
        self.__load_dataset_names()

        # XXX: `__load_dataset_names` initiates the __dataset_dd object. Call it before following code
        # If there are multiple data types construct and show datatype dropdown box
        if len(self.DATA_FILES) > 1:
            datatype_dd = bkm.Select(title="Data Type: ", options=list(self.DATA_FILES.keys()), height=50)
            datatype_dd.on_change("value", self.__datatype_select_callback)

            col = bkl.row(sizing_mode="scale_width", children=[self.__dataset_dd, datatype_dd])
            self.__root.children.append(col)

            curdoc().title = "%s - %s" % (self.data_type, self.TITLE)
        else:
            # Just add the dataset drop down
            self.__root.children.append(self.__dataset_dd)

        self.__refresh_page()

    def update_page_content(self, content):
        """ Update the page content by removing (if it exists) the old element in `:cls:attr:(__root)`
        and adding `content` underneath the data selection UI elements.

        Args:
            content (obj): Object to update the page content with
        """
        if len(self.__root.children) > 1:
            self.__root.children.pop(1)
        self.__root.children.append(content)

    def valid_dataset_name(self, name):
        """ Check if a dataset name is valid, i.e. dosen't leave the target data folder and the folder
        contains the required files.

        Args:
            name (str): Name of the dataset to check
            files (list of str): Files that need to be in the data folder to be valid

        Returns:
            bool: True if the dataset name is valid, false otherwise
        """
        # Make sure the name dosen't leave the working data directory
        cwd_dir = os.path.dirname(os.path.abspath(__file__))
        base_dir = os.path.join(cwd_dir, "DATA")
        data_dir = os.path.abspath(os.path.join(base_dir, name))
        if not data_dir.startswith(base_dir):
            return False

        # Check that the required data elements exists in the data folder
        for f in self.DATA_FILES[self.data_type]:
            if isinstance(f, tuple):
                # Go through the list of potential files and check if any exist
                found = False
                for fsub in f:
                    file = os.path.join(data_dir, fsub)
                    if os.path.exists(file) and os.path.isfile(file):
                        found = True
                        break

                # If neither file exists return false
                if not found:
                    return False
            elif isinstance(f, dict):
                # Validate a sequence of specific types, folders or files
                for fsub,fsub_type in f.items():
                    # If the type is a folder check that it exists
                    if fsub_type == "folder":
                        fsub_path = os.path.join(data_dir, fsub)
                        if not os.path.exists(fsub_path) or not os.path.isdir(fsub_path):
                            return False
                    # If the type is a file check that it exists
                    elif "file":
                        fsub_path = os.path.join(data_dir, fsub)
                        if not os.path.exists(fsub_path) or not os.path.isfile(fsub_path):
                            return False
            else:
                # Check if the single file exists
                file = os.path.join(data_dir, f)
                if not os.path.exists(file) or not os.path.isfile(file):
                    return False

        # Files are there, so we can load data
        return True

    def __dataset_select_callback(self, attr, old, new):
        """ Callback executed when the dataset dropdowns data is changed. Method loads
        the new data.
        """
        if self.dataset != new:
            self.dataset = new
            self.load_data(new)

    def __datatype_select_callback(self, attr, old, new):
        """ Callback executed when the data type dropdown data is changed. Method changes
        the data-type and reloads the page.
        """
        if old != new:
            self.data_type = new
            self.__load_dataset_names()
            self.__refresh_page()

            # Set the title to the selected value
            curdoc().title = "%s - %s" % (self.data_type, self.TITLE)

    def __load_dataset_names(self):
        """ Find all valid dataset names in the data folder, updating `:cls:attr:(dataset_names)`
        and updating/initating the dataset dropdown element `:cls:attr:(__dataset_dd)`.
        """
        self.dataset_names = []
        for child in os.listdir("DATA"):
            if os.path.isdir(os.path.join("DATA", child)):
                if self.valid_dataset_name(child):
                    self.dataset_names.append(child)
        self.dataset_names.sort()

        # Update or initate the the dataset dropdown
        if self.__dataset_dd is None:
            self.__dataset_dd = bkm.Select(title="Dataset: ", options=self.dataset_names, height=50)

            # Set the default dataset if we have dataset values
            if len(self.dataset_names) > 0:
                self.__dataset_dd.value = self.dataset_names[0]
                self.dataset = self.dataset_names[0]

            self.__dataset_dd.on_change("value", self.__dataset_select_callback)
        else:
            self.__dataset_dd.options = self.dataset_names

    def __refresh_page(self):
        """ Refresh the page by either showing a error div if no datasets are avaible, or
        loading the current content with the first element in `:cls:attr:(dataset_names)`.
        """
        if len(self.dataset_names) > 0:
            if self.dataset not in self.dataset_names:
                # If the dataset dosen't exist default to first element (auto-triggers page
                # load).
                self.__dataset_dd.value = self.dataset_names[0]
            else:
                # Make sure the dd stays in sync and load data
                self.__dataset_dd.value == self.dataset
                self.load_data(self.dataset)
        else:
            # No content exists so write an error message
            div = bkm.Div(text="<b>No datasets found for graph. Please add data and reload page!</b>")
            self.update_page_content(div)

    def load_data(self, name):
        """ Abstract method to load dataset and build the content of the page.

        Args:
            name (str): Name of the dataset to load.
        """
        raise NotImplementedError()
