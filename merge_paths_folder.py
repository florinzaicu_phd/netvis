#!/usr/bin/python

# -------------------------------------------------------------
#
# Script that merges multiple YATES data result path folders into
# one folder for processing and plotting. Method takes two or more
# folder paths which it combines to a single target folder. The first
# folder, files are copied without modifications, while the second,
# the name will be modified to append the file index to it. Filter
# only applies to subsequent folders paths (not the first).
#
# Usage:
#   ./merge_paths_folder.py --folders <folder ...> --out <out>
#           --filter [filter]
#
#   <folder ...> = List of folders to merge (two or more)
#
#   <out> = Final merged folder path location
#
#   [filter] = List of algorithm name strings to include in the merged
#              directory. Note that this operation is not applicable
#              to the first folder specified.
#
# -------------------------------------------------------------

from argparse import ArgumentParser
import os
import shutil
import sys
import re


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--folders", required=True, type=str, nargs="+",
        help="List of folder paths to merge")
    parser.add_argument("--out", required=True, type=str, help="Final output merge directory")
    parser.add_argument("--filter", required=False, type=str, default=None, nargs="+",
        help="Only copy files of this algorithm to the merge directory (first not included)")

    # Parse the arguments and make sure the input file is valid and exists
    args = parser.parse_args()
    if len(args.folders) < 2:
        print("Please enter at-lest two folders to merge")
        sys.exit(1)

    for fpath in args.folders:
        if not os.path.exists(fpath) or not os.path.isdir(fpath):
            print("Invalid folder path (%s)" % fpath)
            sys.exit(1)

    print("Starting to merge folders ...")
    if not os.path.exists(args.out):
        os.mkdir(args.out)

    for index in range(len(args.folders)):
        print("\tMerging folder %s" % args.folders[index])

        # If the input folder and output are the same do not perform the first iteration copy
        if os.path.abspath(args.folders[index]) == os.path.abspath(args.out):
            print("\tFirst input folder and output directory are the same, skipping first copy")
            continue

        for f in os.listdir(args.folders[index]):
            target_f = os.path.join(args.folders[index], f)
            if index == 0:
                shutil.copy2(target_f, args.out)
            else:
                reg = re.search("([a-zA-Z0-9_]+)_([0-9]+)", f)
                if reg is None:
                    print("Skipping invalid path file with name %s" % f)
                    continue
                algo = reg.group(1)
                iter = reg.group(2)

                if args.filter is not None and algo not in args.filter:
                    continue

                algo = "%s_%d" % (algo, index)
                new_f = "%s_%s" % (algo, iter)
                dest_f = os.path.join(args.out, new_f)
                shutil.copy2(target_f, dest_f)

    print("Done, merged paths folder files to output %s" % args.out)
