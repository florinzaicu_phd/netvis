#!/user/bin/python
import os
from latency_cdf_graph import generate_page
from base_server import BaseServer


class Graph(BaseServer):
    TITLE = "Average Latency CDF Graph"
    DATA_FILES = {"Default": ["LatencyDistributionVsIterations.dat"]}

    def load_data(self, name):
        data_dir = os.path.abspath(os.path.join("DATA", name))
        input = os.path.join(data_dir, "LatencyDistributionVsIterations.dat")
        layout = generate_page(input)
        self.update_page_content(layout)


# Generate the page
Graph()
