#!/user/bin/python
import sys
import os
import re
import json
import math
from argparse import ArgumentParser

# Visualisation Libraries
from bokeh.io import show, output_file
from bokeh import models as bkm
from bokeh import layouts as bkl
from bokeh import palettes as bkp

# Import helper methods
from graph_helper import init_nx, init_plot, gen_plot_style_controls, get_plot_port_ind


# --------------------------------------------------------
# Graph that shows the number of times a path uses a particular edge in a topology using
# the output paths from YATES. Data displayed for every iteration.
#
# XXX: Normally, the script will go through and process the folder 'paths/', however,
# for some result sets this takes too long so the script now requires pre-compiled path
# information in a JSON file.
#
# Precompile using:
#   'path_edgtes_used.py --topo <topo> --data <result folder> --node_assoc [assoc file] --compile'.
# --------------------------------------------------------


# JS code that is used to show the node and host information on selection of a node
JS_node_select ="""var node_data = n_ds.data;
var ind = n_ds.selected["1d"]["indices"][0];
var i_ds = i_ds.data;
div = document.getElementById("embed");

if (ind == null) {
    div.innerHTML = "Click on node to view edge info!";
    return;
}

var node_country = node_data["Country"][ind];
var node_label = node_data["label"][ind];
var node_host = "N/A";
if ("Host" in node_data)
    node_host = node_data["Host"][ind];

var node_transit_in = node_data["transit_in"][ind];
var node_transit_out = node_data["transit_out"][ind];
var node_src = node_data["src"][ind];
var node_dst = node_data["dest"][ind];

div.innerHTML = "<strong>Selected Node Info</strong><br />";
div.innerHTML += "<em>ID:</em> "+ind+"<br />";
div.innerHTML += "<em>Label:</em> "+node_label+"<br />";
div.innerHTML += "<em>Country:</em> "+node_country+"<br />";
div.innerHTML += "<em>Host:</em> "+node_host+"<br />";
div.innerHTML += "<em>Used Transit In:</em> "+node_transit_in+"<br />";
div.innerHTML += "<em>Used Transit Out:</em> "+node_transit_out+"<br />";
div.innerHTML += "<em>Total Used Host Source:</em> "+node_src+"<br />";
div.innerHTML += "<em>Total Used Host Destination:</em> "+node_dst+"<br />";

div.innerHTML += "<br /><strong>Selected Node Edges Info</strong><br />";

for (var i = 0; i < i_ds["from"].length; i++) {
    if (i_ds["from"][i] == node_data["index"][ind]) {
        div.innerHTML += "<hr /><strong>Link Info</strong><br />";
        div.innerHTML += "<em>ID:</em> "+i+"<br />";
        div.innerHTML += "<em>From:</em> "+i_ds["from"][i]+"<br />";
        div.innerHTML += "<em>To:</em> "+i_ds["to"][i]+"<br />";
        div.innerHTML += "<em>Used by Path:</em> "+i_ds["u_pri"][i]+"<br />";
    }
}
"""

JS_select_path_data = ("""algo = algo.value;
if (iter != null) {
    iter = iter.value;
} else {
    for (key in d[algo]) {
        iter = key
        break;
    }
}
var data = d[algo][iter]["edge"];

plot.title.text = "Path Edges Used - Algorithm " + algo + " iteration " + iter;
i_ds.data["u_pri"] = data;

for (target in d[algo][iter]["node"]) {
    var target_ds = d[algo][iter]["node"][target];
    for (var i = 0; i < n_ds.data["index"].length; i++) {
        if (n_ds.data["index"][i] == target) {
            n_ds.data["transit_in"][i] = target_ds["transit_in"];
            n_ds.data["transit_out"][i] = target_ds["transit_out"];
            n_ds.data["src"][i] = target_ds["src"];
            n_ds.data["dest"][i] = target_ds["dest"];
            break;
        }
    }
}

for (var i = 0; i < e_ds.data["start"].length; i++) {
    e_ds.data["color"][i] = id_info["edge_default"]["color"];
}
for (var i = 0; i < n_ds.data["id"].length; i++) {
    n_ds.data["color"][i] = id_info["node_default"]["color"];
}

if (algo.includes("HelixMC")) {
    for (var i = 0; i < e_ds.data["start"].length; i++) {
        test_idp = e_ds.data["start"][i] + "-" + e_ds.data["end"][i];
        if (id_info["link"].includes(test_idp)) {
            e_ds.data["color"][i] = "#ff0000";
        }
    }

    for (var i = 0; i < n_ds.data["id"].length; i++) {
        test_node = n_ds.data["id"][i];
        if (test_node in id_info["node"]) {
            n_ds.data["color"][i] = id_info["node"][test_node];
        }
    }
}

i_ds.change.emit();
n_ds.change.emit();
e_ds.change.emit();
"""
+ JS_node_select)

def select_path_data(path_data, info_ds, node_ds, edge_ds, id_info, algo, iter):
    """ Changes the path data currently being shown, triggering a UI update.
    The method will go through the processed path information, updating the display
    data source to show algorithm `algo` and iteration `iter`.

    Args:
        path_data (dict): Raw data loaded from the path files
        info_ds (ColumnDataSource): Display datasource that needs to be updated
        node_ds (ColumnDataSource): Display node datasource to add node aggregate info
        edge_ds (ColumnDataSource): Display edge datasource to show IDP links for HelixMC
        id_info (dict): Inter-domain information to show for HelixMC
        algo (string): Algorithm to load data for
        iter (int): Iteration of algorithm data to show
    """
    # Retrieve the raw data dictionary to process and go through link keys
    data = path_data[algo][iter]["edge"]

    # Iterate through nodes and update their info
    for target,target_ds in path_data[algo][iter]["node"].items():
        for i in range(len(node_ds.data["index"])):
            if node_ds.data["index"][i] == target:
                node_ds.data["transit_in"][i] = target_ds["transit_in"]
                node_ds.data["transit_out"][i] = target_ds["transit_out"]
                node_ds.data["src"][i] = target_ds["src"]
                node_ds.data["dest"][i] = target_ds["dest"]
                break

    # Reset the color of the edges and nodes
    for i in range(len(edge_ds.data["start"])):
        edge_ds.data["color"][i] = id_info["edge_default"]["color"]
    for i in range(len(node_ds.data["id"])):
        node_ds.data["color"][i] = id_info["node_default"]["color"]

    # Highlight inter-domain nodes and links if algorithm is HelixMC
    if "HelixMC" in algo:
        # Update the color of the inter-domain edges
        for i in range(len(edge_ds.data["start"])):
            test_idp = "%s-%s" % (edge_ds.data["start"][i], edge_ds.data["end"][i])
            if test_idp in id_info["link"]:
                edge_ds.data["color"][i] = "#ff0000"

        # Color the nodes for different domains
        for i in range(len(node_ds.data["id"])):
            test_node = node_ds.data["id"][i]
            if test_node in id_info["node"]:
                node_ds.data["color"][i] = id_info["node"][test_node]

    info_ds.data["u_pri"] = data

def inc_edge_used(path, target_ds, key_ds, node_ds, assoc_data):
    """ Increment the number of times a path uses an edge of a topology """
    path_hops = path.split(")")
    for hop in path_hops:
        if hop == "":
            continue

        hop = hop.split("(")[1]
        nodes = hop.split(",")
        n_to = nodes[0].strip()
        n_from = nodes[1].strip()

        # If there are no hosts in the hop increment node usages as transit
        if "h" not in n_to and "h" not in n_from:
            if n_from not in node_ds:
                node_ds[n_from] = {"transit_in": 0, "transit_out": 0, "src": 0, "dest": 0}
            node_ds[n_from]["transit_in"] += 1

            if n_to not in node_ds:
                node_ds[n_to] = {"transit_in": 0, "transit_out": 0, "src": 0, "dest": 0}
            node_ds[n_to]["transit_out"] += 1
        else:
            if "h" in n_from:
                if n_to not in node_ds:
                    node_ds[n_to] = {"transit_in": 0, "transit_out": 0, "src": 0, "dest": 0}
                node_ds[n_to]["src"] += 1

            if "h" in n_to:
                if n_from not in node_ds:
                    node_ds[n_from] = {"transit_in": 0, "transit_out": 0, "src": 0, "dest": 0}
                node_ds[n_from]["dest"] += 1

        if n_to in assoc_data:
            n_to = assoc_data[n_to]
        if n_from in assoc_data:
            n_from = assoc_data[n_from]

        # Increment the edge used data
        for q in range(len(key_ds["to"])):
            if key_ds["from"][q] == n_to and key_ds["to"][q] == n_from:
                target_ds[q] += 1
                break

def compile_data(topo_input, result_folder, node_assoc=None):
    """ Compile a path folder dataset to a path edges used JSON file

    Args:
        topo_input (str): Path to the topology file used to collect results
        result_folder (str): Path to result folder we need to compile
        node_assoc (str): Optional path to a association JSON to map a node
            label to another label
    """
    # Load the topology data and initiate the info DS
    info_ds = {
        "from": [],
        "to": [],
        "u_pri": [],
        "xs": [],
        "ys": []
    }
    g, pos, info_ds = init_nx(topo_input, info_ds, None, lambda d: (d["u_pri"].append(0)))

    assoc_data = {}
    if node_assoc is not None:
        with open(node_assoc, "r") as fin:
            assoc_data = json.loads(fin.read())

    # Get a list of all algorithms and the number of iterstions
    path_data = {}
    path_input = os.path.join(result_folder, "paths")
    for f in os.listdir(path_input):
        reg = re.search("([a-zA-Z0-9_]+)_(\d+)", f)
        if reg is None:
            continue
        algo = reg.group(1)
        iteration = int(reg.group(2))

        if algo not in path_data:
            path_data[algo] = {}
        if iteration not in path_data[algo]:
            path_data[algo][iteration] = {"edge": ([0] * len(info_ds.data["from"])), "node": {}}

        # Process the path file to a liust of path usages
        with open(os.path.join(path_input, f), "r") as fin:
            for line in fin:
                line = line.strip()
                # Check if the line is a comment or empty
                if line == "" or line.startswith("#"):
                    continue
                # Check if this is a key pair line
                elif "->" in line:
                    # Split the tokens in the data line
                    reg = re.search("([a-zA-Z0-9-]+)\s*->\s*([a-zA-Z0-9-]+)\s*:", line)
                    kfrom = reg.group(1)
                    kto = reg.group(2)

                    if kfrom in assoc_data:
                        kfrom = assoc_data[kfrom]
                    if kto in assoc_data:
                        kto = assoc_data[kto]
                    key = "%s-%s" % (kfrom, kto)
                # Otherwise check for a path line
                else:
                    # Process the algo and iter
                    reg = re.search("\[((?:\([a-zA-Z0-9-]+,[a-zA-Z0-9-]+\),*\s*)+)\]\s*@\s*[0-9.]+", line)
                    if reg is None:
                        continue
                    path = reg.group(1)

                    # Increment the edges
                    inc_edge_used(path, path_data[algo][iteration]["edge"], info_ds.data,
                                    path_data[algo][iteration]["node"], assoc_data)

    # Output the compiled paths to a json file
    with open(os.path.join(result_folder, "path_edges_used.json"), "w") as fout:
        json.dump(path_data, fout)

def generate_page(topo_input, path_input, node_assoc=None, sw_ctrl_map=None, host_name=None):
    """ Generate and return page layout showing data from `path_input` on topology
    `topo_input`. The graph shows the number of times each port of the topology is
    used by the computed path

    Args:
        topo_input (str): Path to the topology file used to collect results
        path_input (str): Path to YATES path output file
        node_assoc (str): Optional path to a association JSON to map a node
            label to another label
        sw_ctrl_map (str): Optnail switch to controller map file to show inter-domain links
            for HelixMC algorithm
        host_name (str): Optional path to JSON file that describes host
            name of labels (prevents auto generation with node IDs).

    Returns:
        obj: Page layout to display
    """
    # Load the host name data from the provided file
    host_names  = None
    if host_name is not None:
        with open(host_name, "r") as fin:
            host_names = json.loads(fin.read())

    # Load the topology data and initiate the info DS
    info_ds = {
        "from": [],
        "to": [],
        "u_pri": [],
        "xs": [],
        "ys": []
    }
    g, pos, info_ds = init_nx(topo_input, info_ds, node_extra={"transit_in": 0,
                                "transit_out": 0, "src": 0, "dest": 0},
                                edge_init=lambda d: (d["u_pri"].append(0)),
                                host_names=host_names)

    # XXX: Because data is compiled we only need to re-assoc node information as edge data has
    # already been processed
    assoc_data = {}
    if node_assoc is not None:
        with open(node_assoc, "r") as fin:
            assoc_data = json.loads(fin.read())

    # Load the path edge used data file and modify the iteration key to be an interger
    path_data = {}
    with open(path_input, "r") as fin:
        tmp_data = json.loads(fin.read())
        for algo,algo_d in tmp_data.items():
            path_data[algo] = {}
            for iter,iter_d in algo_d.items():
                # Convert the iteration key to a integer
                path_data[algo][int(iter)] = {"edge": [], "node": {}}
                path_data[algo][int(iter)]["edge"] = iter_d["edge"]

                # Clean up and associate node information
                for node,node_d in iter_d["node"].items():
                    if node in assoc_data:
                        node = assoc_data[node]
                    path_data[algo][int(iter)]["node"][node] = node_d

    # Generate total usage and work out MAX paths for graph scale
    max_paths = 0
    for algo,algo_d in path_data.items():
        for iter,iter_d in algo_d.items():
            for count in iter_d["edge"]:
                if count > max_paths:
                    max_paths = count

    # Make max paths a multiple of 20
    max_paths = int(math.ceil(max_paths / 20.0)) * 20


    # -------------------- GENERATE GRAPH -------------------


    # Generate the plot and renderers
    plot, graph_ren = init_plot("Path Edges Used", g, pos, port_ind_ds=info_ds,
                                port_ind_field="u_pri", port_ind_scale=(0, max_paths),
                                port_ind_title="# Paths")
    node_ren = graph_ren.node_renderer
    edge_ren = graph_ren.edge_renderer
    node_ds = node_ren.data_source
    edge_ds = edge_ren.data_source

    callback_node = bkm.CustomJS(args=dict(n_ds=node_ds, i_ds=info_ds), code=JS_node_select)
    node_ds.selected.js_on_change('indices', callback_node)

    # Go through and process the idp links information
    inter_dom_info = {"link": [], "node": {},
        "edge_default": {"color": "#cccccc", "sel_color": bkp.Spectral4[2]},
        "node_default": {"color": bkp.Spectral4[0], "sel_color": bkp.Spectral4[2]}
    }
    domains = []
    if sw_ctrl_map is not None:
        with open(sw_ctrl_map, "r") as fin:
            tmp = json.loads(fin.read())
            for ctrl,ctrl_d in tmp["ctrl"].items():
                # Process the switches of the domain
                for sw in ctrl_d["sw"]:
                    if ctrl not in domains:
                        domains.append(ctrl)
                    i = domains.index(ctrl)
                    if sw in assoc_data:
                        sw = assoc_data[sw]
                    inter_dom_info["node"][sw] = bkp.Pastel1[9][i]

                # Process the inter-domain links
                for dom,dom_d in ctrl_d["dom"].items():
                    for info in dom_d:
                        info_from = info["sw"]
                        info_to = info["sw_to"]
                        if info_from in assoc_data:
                            info_from = assoc_data[info_from]
                        if info_to in assoc_data:
                            info_to = assoc_data[info_to]

                        idp = "%s-%s" % (info_from, info_to)
                        if idp not in inter_dom_info["link"]:
                            inter_dom_info["link"].append(idp)


    # -------------------- DISPLAY CONTROLS ---------------------


    # Create container to show info of clicked nodes and connected links
    embed_div = bkm.Div(text="<div id='embed'>Click on node to view edge info!</div>",
        style={"padding": "20px", "width": "80%", "background": "#eee", "box-sizing":
                "border-box"})

    # Create controls to allow changing algo and iter being shown
    algo_ls = list(path_data.keys())
    start = algo_ls[0]
    algo_dd = bkm.Select(title="Algorithm: ", value=start, options=algo_ls, height=50,
                            sizing_mode="stretch_width")

    iter_ls = list(path_data[start].keys())
    iter_ls.sort()
    start = int(iter_ls[0])
    end = int(iter_ls[len(iter_ls)-1])
    iter_sl = None
    if not start == end:
        iter_sl = bkm.Slider(start=start, end=end, step=1, value=start, title="Iteration",
                            height=50, sizing_mode="stretch_width")

    # Prime with default data and bind callback to albo and iter slides to change data shown
    select_path_data(path_data, info_ds, node_ds, edge_ds, inter_dom_info, algo_dd.value, start)
    ui_change_callback = bkm.CustomJS(args=dict(d=path_data, i_ds=info_ds, n_ds=node_ds,
                                        e_ds=edge_ds, id_info=inter_dom_info,
                                        algo=algo_dd, iter=iter_sl, plot=plot),
                                        code=JS_select_path_data)
    algo_dd.js_on_change('value', ui_change_callback)
    if iter_sl is not None:
        iter_sl.js_on_change('value', ui_change_callback)

    # Update the plot title
    plot.title.text = "Path Edges Used - Algorithm %s iteration %s" % (algo_dd.value, start)


    # -------------------- CHART UI AND SHOW GRAPH ---------------------


    # Configure the output file attributes, built the page layout and show the graph
    opt_layout = bkl.column(children=gen_plot_style_controls(plot), width=120)
    main_layout = bkl.row(children=[opt_layout, bkl.Spacer(width=10), plot])

    # If the iteration slider should not be displayed do not add it to page
    ctrl_elements = [main_layout, algo_dd]
    if iter_sl is not None:
        ctrl_elements.append(iter_sl)
    ctrl_elements.append(embed_div)

    page_layout = bkl.column(children=ctrl_elements, sizing_mode="scale_both",
                                margin=(5,20,5,5))
    return page_layout

if __name__ == "__main__":
    # Initiate the argument parser and validate arguments
    parser = ArgumentParser("Path Edges Used Graph")
    parser.add_argument("--topo", required=True, type=str,
            help="Topology file (.dot or .gml)")
    parser.add_argument("--data", required=True, type=str,
            help="Path to data file or paths to result folder when compiling")
    parser.add_argument("--node_assoc", required=False, type=str, default=None,
            help="Node association JSON file (optional)")
    parser.add_argument("--sw_ctrl_map", required=False, type=str, default=None,
            help="HelixMC switch to controller map file (optional)")
    parser.add_argument("--host_name", required=False, type=str, default=None,
            help="JSON File that specifies host names for nodes (optional)")
    parser.add_argument("--compile", required=False, action="store_true",
            help="Compile paths data folder to JSON file (does not display UI)")
    args = parser.parse_args()

    if not os.path.exists(args.topo) or not os.path.isfile(args.topo):
        print("Please enter a valid topology file!")
        exit(1)

    # Check if we need to just compile data
    if args.compile:
        # Make sure paths folder path is specified and valid
        if not os.path.exists(args.data) or not os.path.isdir(args.data):
            print("Please enter valid paths data folder for compile operation")
            exit(1)

        paths_folder = os.path.join(args.data, "paths")
        if not os.path.exists(paths_folder) or not os.path.isdir(paths_folder):
            print("Data folder dosen't contain paths/ folder. Can't compile!")
            exit(1)
    else:
        if not os.path.exists(args.data) or not os.path.isfile(args.data):
            print("Please enter a valid data file")
            exit(1)

    # If the node assoc file is invalid do not load it
    if (args.node_assoc is None or not os.path.exists(args.node_assoc)
            or not os.path.isfile(args.node_assoc)):
        args.node_assoc = None

    # If the switch to controller map argument is invalid do not load it
    if (args.sw_ctrl_map is None or not os.path.exists(args.sw_ctrl_map)
            or not os.path.isfile(args.sw_ctrl_map)):
        args.sw_ctrl_map = None

    # If the host name file argument is invalid do not load it
    if (args.host_name is None or not os.path.exists(args.host_name)
            or not os.path.isfile(args.host_name)):
        args.host_name = None

    output_file("path_edges_used_graph.html", title="Path Edges Used Graph")
    # If the operation is compile process data and output to JSON file
    if args.compile:
        print("Compiling paths folder to path edges used data file")
        print("\tResult Folder: %s" % args.data)
        print("\tOutput File : %s" % os.path.join(args.data, "path_edges_used.json"))

        compile_data(args.topo, args.data, args.node_assoc)
        print("\tFinished operation!\n")
        print("Restart script without compile flag")
    else:
        show(generate_page(args.topo, args.data, args.node_assoc, args.sw_ctrl_map,
                            args.host_name))
