#!/user/bin/python
import os
from congestion_graph import generate_page
from base_server import BaseServer


class Graph(BaseServer):
    TITLE = "Congestion Graph"

    DATA_FILES = {
        "Mean": ["MeanCongestionVsIterations.dat"],
        "Max": ["MaxCongestionVsIterations.dat"],
        "Loss": ["CongestionLossVsIterations.dat"],
        "Mean Exponential": ["MeanExpCongestionVsIterations.dat"],
        "Max Exponential": ["MaxExpCongestionVsIterations.dat"],
    }

    DEFAULT_DATA_TYPE = "Mean"

    def load_data(self, name):
        data_dir = os.path.abspath(os.path.join("DATA", name))

        if self.data_type == "Mean":
            input = os.path.join(data_dir, "MeanCongestionVsIterations.dat")
        elif self.data_type == "Max":
            input = os.path.join(data_dir, "MaxCongestionVsIterations.dat")
        elif self.data_type == "Loss":
            input = os.path.join(data_dir, "CongestionLossVsIterations.dat")
        elif self.data_type == "Mean Exponential":
            input = os.path.join(data_dir, "MeanExpCongestionVsIterations.dat")
        elif self.data_type == "Max Exponential":
            input = os.path.join(data_dir, "MaxExpCongestionVsIterations.dat")

        layout = generate_page(input)
        self.update_page_content(layout)


# Generate the page
Graph()
