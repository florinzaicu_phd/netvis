#!/user/bin/python
import sys
import os
import re
from argparse import ArgumentParser

# Visualisation Libraries
from bokeh.io import show, output_file
from bokeh import models as bkm
from bokeh import layouts as bkl
from bokeh import palettes as bkp


# --------------------------------------------------------
# Graph that shows TE optimisation request and failures for the HelixMC
# data (shows TE optimisation stats for each controller instance)
# --------------------------------------------------------


JS_SELECT_SHOW_DATA = """
plot.title.text = "Multi-Controller TE Optimisation Request State - " + algo + " (" + cid + ")";
for (var i = 0; i < data["total"].length; i++) {
    ds_dict["total"].data["bottom"][i] = 0;
    ds_dict["total"].data["top"][i] = data["total"][i];
}

for (var i = 0; i < data["total"].length; i++) {
    ds_dict["success"].data["bottom"][i] = 0;
    ds_dict["success"].data["top"][i] = data["success"][i];
}

ds_dict["total"].change.emit();
ds_dict["success"].change.emit();
"""

JS_SELECT_ALGO_DATA = (""" var algo = cb_obj.value;
var cid = cid_dd.value;
var data = stats_data[algo][cid];
""" + JS_SELECT_SHOW_DATA)

JS_SELECT_CID_DATA = (""" var cid = cb_obj.value;
var algo = algo_dd.value;
var data = stats_data[algo][cid];
""" + JS_SELECT_SHOW_DATA)

# ONLY SHOW RESULTS FOR THE FOLLOWING ALGORITHMS (IF THEY EXIST IN THE DATA FILE)
ALGO_FILTER = ["Helix", "HelixMC"]

def select_algo_data(stats_data, algo, cid, ds_dict, plot):
    """ Show algorithm TE Optimisation count state.

    Args:
        stats_data (dict): Raw data loaded from stats result file
        algo (str): Algorithm to show data of
        cid (str): Controller to show data of
        ds_dict (dict): Dictionary of data sources for the bars shown on the graph
        plot (bkm.Plot): Plot to show data
    """
    data = stats_data[algo][cid]

    # Update the plot tile
    plot.title.text = "Multi-Controller TE Optimisation Request State - %s (%s)" % (algo, cid)

    # Update the DS with the selected data
    for i in range(len(data["total"])):
        ds_dict["total"].data["bottom"][i] = 0
        ds_dict["total"].data["top"][i] = data["total"][i]

    for i in range(len(data["total"])):
        ds_dict["success"].data["bottom"][i] = 0
        ds_dict["success"].data["top"][i] = data["success"][i]

def generate_page(input):
    """ Generate and return the page layout showing the data from `input`. Latency
    is displayed as a stacked plot where each value shows the percentage of total
    traffic using a path of length n to reach it's destination.

    Returns:
        obj: Page layout to display
    """
    stats_data = {}
    max_iteration = 0
    max_opti_count = 0
    algo_ls = []
    cid_ls = []
    with open(input, "r") as fin:
        algo = []
        iter = 0
        # Iterate through the line of the file
        for line in fin:
            line = line.strip()

            # Skip comment or empty lines
            if line == "" or line.startswith("#"):
                continue

            # If this is a result line process it
            elif ":" in line:
                for AFILT in ALGO_FILTER:
                    if AFILT not in algo:
                        # Only process algorithms we need to
                        continue

                reg = re.search("\s*\((\w+)\)\s*:\s*(\d*)\s*(\d*)", line)
                if not reg:
                    # If the regex dosen't match the data line skip it
                    continue

                cid = reg.group(1)
                success = int(reg.group(2))
                failure = int(reg.group(3))
                total = success + failure

                # Prime the dict with default values, if new algo
                if cid not in cid_ls:
                    cid_ls.append(cid)
                if cid not in stats_data[algo]:
                    stats_data[algo][cid] = {"raw": []}

                stats_data[algo][cid]["raw"].append(
                    {"success": success, "fail": failure, "total": total}
                )

                # Check if we found a new max iter and initiate the data list
                if iter > max_iteration:
                    max_iteration = iter
                if max_opti_count < total:
                    max_opti_count = total

            # Otherwise assume it's a algorithm iteration line
            else:
                reg = re.search("([a-zA-Z0-9_]+)\s([0-9]+)", line)
                algo = reg.group(1)
                iter = int(reg.group(2))

                # Initiate algorithm if it dosen't exist already
                for AFILT in ALGO_FILTER:
                    if AFILT in algo:
                        if algo not in algo_ls:
                            algo_ls.append(algo)
                        if algo not in stats_data:
                            stats_data[algo] = {}

    for algo in algo_ls:
        for cid in cid_ls:
            # If the CID dosen't exist in the algorithm remove the jagged algo data
            if cid not in stats_data[algo]:
                print("Removing jagged algo data %s" % algo)
                del stats_data[algo]
                break

            stats_data[algo][cid]["success"] = []
            stats_data[algo][cid]["total"] = []

            for i in range(max_iteration):
                stats_data[algo][cid]["success"].append(stats_data[algo][cid]["raw"][i]["success"])
                stats_data[algo][cid]["total"].append(stats_data[algo][cid]["raw"][i]["total"])

            del stats_data[algo][cid]["raw"]

    # Re-get the algorithm list from the data (may have removed an algorithm due to cleanup)
    algo_ls = list(stats_data.keys())

    # If there is no valid data to show show a warning message
    if len(algo_ls) == 0 or len(cid_ls) == 0:
        return bkm.Div(text="<b>Selected dataset has no data. Please add data and reload page</b>")

     # Initiate the plot and axis
    plot = bkm.Plot(toolbar_location="right", sizing_mode="stretch_both")
    x_axis = bkm.LinearAxis(bounds=(0, max_iteration), axis_label="Iteration Number")
    plot.add_layout(x_axis, 'below')
    y_axis = bkm.LinearAxis(bounds=(0, max_opti_count), axis_label="# Requests")
    plot.add_layout(y_axis, 'left')
    plot.add_layout(bkm.Grid(dimension=0, ticker=x_axis.ticker))
    plot.add_layout(bkm.Grid(dimension=1, ticker=y_axis.ticker))

    # Generate the graph datasource
    colors = bkp.Category20[7]
    ds_dict = {"iterations": list(range(max_iteration+1))}
    litems = []

    # --- TOTAL DATASOURCE ---
    top = [0] * (max_iteration + 1)
    bottom = [0] * (max_iteration + 1)
    ds_dict["total"] = bkm.ColumnDataSource(dict(x=ds_dict["iterations"], top=top, bottom=bottom))
    glyph = bkm.VBar(x="x", top="top", bottom="bottom", width=1, fill_color=colors[6])
    ren = bkm.GlyphRenderer(name="total", data_source=ds_dict["total"], glyph=glyph, visible=True)
    plot.renderers.append(ren)
    litem = bkm.LegendItem(label="Total", renderers=[ren])
    litems.append(litem)

    # --- SUCCESS DATASOURCE ---
    top = [0] * (max_iteration + 1)
    bottom = [0] * (max_iteration + 1)
    ds_dict["success"] = bkm.ColumnDataSource(dict(x=ds_dict["iterations"], top=top, bottom=bottom))
    glyph = bkm.VBar(x="x", top="top", bottom="bottom", width=1, fill_color=colors[4])
    ren = bkm.GlyphRenderer(name="success", data_source=ds_dict["success"], glyph=glyph, visible=True)
    plot.renderers.append(ren)
    litem = bkm.LegendItem(label="Success", renderers=[ren])
    litems.append(litem)

    # Initiate the legend, add it under the plot and load the default data
    legend = bkm.Legend(items=litems, location="center", orientation="horizontal",
                        background_fill_alpha=1.0)
    plot.add_layout(legend, "below")
    select_algo_data(stats_data, algo_ls[0], cid_ls[0], ds_dict, plot)

    # Add tools to the plot to allow interaction
    node_hover_format = bkm.CustomJSHover(code="return special_vars.name;")
    hover_tl = bkm.HoverTool(show_arrow=False, tooltips=[("Iteration", "@x"), ("Start", "@bottom"),
                                ("End", "@top"), ("Nodes", "@x{custom}")],
                                formatters=dict(x=node_hover_format))
    zoom_tl = bkm.WheelZoomTool()
    plot.toolbar.active_scroll = zoom_tl
    plot.add_tools(hover_tl, bkm.TapTool(), zoom_tl, bkm.PanTool(), bkm.ResetTool(), bkm.SaveTool())

    # Add an algo selection and controller selection box
    cid_dd = bkm.Select(title="Controller: ", value=cid_ls[0], options=cid_ls, height=50,
                            sizing_mode="stretch_width")
    algo_dd = bkm.Select(title="Algorithm: ", value=algo_ls[0], options=algo_ls, height=50,
                            sizing_mode="stretch_width")

    algo_change_callback = bkm.CustomJS(args=dict(stats_data=stats_data, ds_dict=ds_dict, cid_dd=cid_dd,
                                        max_iteration=max_iteration, plot=plot), code=JS_SELECT_ALGO_DATA)
    algo_dd.js_on_change('value', algo_change_callback)
    cid_change_callback = bkm.CustomJS(args=dict(stats_data=stats_data, ds_dict=ds_dict, algo_dd=algo_dd,
                                        max_iteration=max_iteration, plot=plot), code=JS_SELECT_CID_DATA)
    cid_dd.js_on_change('value', cid_change_callback)

    # Configure the output file attributes, built the page layout and show the graph
    row_sel = bkl.row(children=[algo_dd, cid_dd], sizing_mode="scale_width")
    page_layout = bkl.column(children=[plot, row_sel], sizing_mode="scale_both", margin=(5,20,5,5))
    return page_layout

if __name__ == "__main__":
    # Initiate the argument parser and validate arguments
    parser = ArgumentParser("HelixMC TE Opti Req")
    parser.add_argument("--data", required=True, type=str, help="YATES HelixMC TE opti req file")
    args = parser.parse_args()

    if not os.path.exists(args.data) or not os.path.isfile(args.data):
        print("Please enter a valid HelixMC TE opti request file")
        exit(1)

    output_file("helixMC_te_opti_req_graph.html", title="HelixMC TE Optimisation Requests Graph")
    show(generate_page(args.data))
