#!/bin/bash

python path_edges_used.py --topo TMP_minover_swctrlV1/topo.gml --data TMP_minover_swctrlV1/ --node_assoc TMP_minover_swctrlV1/node_assoc.json --compile
python path_edges_used.py --topo TMP_minover_swctrlV2/topo.gml --data TMP_minover_swctrlV2/ --node_assoc TMP_minover_swctrlV2/node_assoc.json --compile
python path_edges_used.py --topo TMP_minover_swctrlV3/topo.gml --data TMP_minover_swctrlV3/ --node_assoc TMP_minover_swctrlV3/node_assoc.json --compile
python path_edges_used.py --topo TMP_minover_swctrlV4/topo.gml --data TMP_minover_swctrlV4/ --node_assoc TMP_minover_swctrlV4/node_assoc.json --compile
python path_edges_used.py --topo TMP_minover_swctrlV5/topo.gml --data TMP_minover_swctrlV5/ --node_assoc TMP_minover_swctrlV5/node_assoc.json --compile
python path_edges_used.py --topo TMP_minover_swctrlV6/topo.gml --data TMP_minover_swctrlV6/ --node_assoc TMP_minover_swctrlV6/node_assoc.json --compile
python path_edges_used.py --topo TMP_minover_swctrlV7/topo.gml --data TMP_minover_swctrlV7/ --node_assoc TMP_minover_swctrlV7/node_assoc.json --compile
python path_edges_used.py --topo TMP_minover_swctrlV8/topo.gml --data TMP_minover_swctrlV8/ --node_assoc TMP_minover_swctrlV8/node_assoc.json --compile
python path_edges_used.py --topo TMP_minover_swctrlV9/topo.gml --data TMP_minover_swctrlV9/ --node_assoc TMP_minover_swctrlV9/node_assoc.json --compile
