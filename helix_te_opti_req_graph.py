#!/user/bin/python
import sys
import os
import re
from argparse import ArgumentParser

# Visualisation Libraries
from bokeh.io import show, output_file
from bokeh import models as bkm
from bokeh import layouts as bkl
from bokeh import palettes as bkp


# --------------------------------------------------------
# Graph that shows TE optimisation request and failures for the helix ctrl
# --------------------------------------------------------


JS_SELECT_ALGO_DATA = """ var algo = cb_obj.value;
var data = stats_data[algo];
plot.title.text = "TE Optimisation Request State - " + algo;
for (var i = 0; i < data["total"].length; i++) {
    ds_dict["total"].data["bottom"][i] = 0;
    ds_dict["total"].data["top"][i] = data["total"][i];
}

for (var i = 0; i < data["total"].length; i++) {
    ds_dict["success"].data["bottom"][i] = 0;
    ds_dict["success"].data["top"][i] = data["success"][i];
}

ds_dict["total"].change.emit();
ds_dict["success"].change.emit();
"""

# ONLY SHOW RESULTS FOR THE FOLLOWING ALGORITHMS (IF THEY EXIST IN THE DATA FILE)
ALGO_FILTER = ["Helix", "HelixMC"]


def select_algo_data(stats_data, algo, ds_dict, plot):
    """ Show algorithm TE Optimisation count state.

    Args:
        stats_data (dict): Raw data loaded from stats result file
        algo (str): Algorithm to show data of
        ds_dict (dict): Dictionary of data sources for the bars shown on the graph
        plot (bkm.Plot): Plot to show data
    """
    data = stats_data[algo]

    # Update the plot tile
    plot.title.text = "TE Optimisation Request State - %s" % algo

    # Update the DS with the selected data
    for i in range(len(data["total"])):
        ds_dict["total"].data["bottom"][i] = 0
        ds_dict["total"].data["top"][i] = data["total"][i]

    for i in range(len(data["total"])):
        ds_dict["success"].data["bottom"][i] = 0
        ds_dict["success"].data["top"][i] = data["success"][i]

def generate_page(input):
    """ Generate and return the page layout showing the data for `input`. The graph
    shows a stacked plot for the number of sucesfull and total  TE optimisation
    requests for the helix controller.

    Returns:
        obj: Page layout to display.
    """
    stats_data = {}
    max_iteration = 0
    max_opti_count = 0
    algo_ls = []
    with open(input, "r") as fin:
        iter = 0
        for line in fin:
            line = line.strip()

            # Skip comment or empty lines
            if line == "" or line.startswith("#"):
                continue

            # Extract the data fields
            reg = re.search("([a-zA-Z0-9_]+)\s([0-9]+)\s([0-9]+)\s([0-9]+)", line)
            algo = reg.group(1)
            iter = int(reg.group(2))
            succ_count = int(reg.group(3))
            fail_count = int(reg.group(4))
            totl_count = succ_count + fail_count

            for AFILT in ALGO_FILTER:
                if AFILT in algo:
                    if algo not in stats_data:
                        stats_data[algo] = {"raw": []}
                        algo_ls.append(algo)

                    stats_data[algo]["raw"].append(
                        {"success": succ_count, "fail": fail_count, "total": totl_count}
                    )

                    if max_iteration < iter:
                        max_iteration = iter
                    if max_opti_count < totl_count:
                        max_opti_count = totl_count

    # Process and clean up the data
    for algo in algo_ls:
        stats_data[algo]["success"] = []
        stats_data[algo]["total"] = []

        for i in range(max_iteration):
            stats_data[algo]["success"].append(stats_data[algo]["raw"][i]["success"])
            stats_data[algo]["total"].append(stats_data[algo]["raw"][i]["total"])

        del stats_data[algo]["raw"]

    # If there is no valid data to show show a warning message
    if len(algo_ls) == 0:
        return bkm.Div(text="<b>Selected dataset has no data. Please add data and reload page</b>")

    # Initiate the plot and axis
    plot = bkm.Plot(toolbar_location="right", sizing_mode="stretch_both")
    x_axis = bkm.LinearAxis(bounds=(0, max_iteration), axis_label="Iteration Number")
    plot.add_layout(x_axis, 'below')
    y_axis = bkm.LinearAxis(bounds=(0, max_opti_count), axis_label="# Requests")
    plot.add_layout(y_axis, 'left')
    plot.add_layout(bkm.Grid(dimension=0, ticker=x_axis.ticker))
    plot.add_layout(bkm.Grid(dimension=1, ticker=y_axis.ticker))

    # Generate the graph datasource
    colors = bkp.Category20[7]
    ds_dict = {"iterations": list(range(max_iteration+1))}
    litems = []

    # --- TOTAL DATASOURCE ---
    top = [0] * (max_iteration + 1)
    bottom = [0] * (max_iteration + 1)
    ds_dict["total"] = bkm.ColumnDataSource(dict(x=ds_dict["iterations"], top=top, bottom=bottom))
    glyph = bkm.VBar(x="x", top="top", bottom="bottom", width=1, fill_color=colors[6])
    ren = bkm.GlyphRenderer(name="total", data_source=ds_dict["total"], glyph=glyph, visible=True)
    plot.renderers.append(ren)
    litem = bkm.LegendItem(label="Total", renderers=[ren])
    litems.append(litem)

    # --- SUCCESS DATASOURCE ---
    top = [0] * (max_iteration + 1)
    bottom = [0] * (max_iteration + 1)
    ds_dict["success"] = bkm.ColumnDataSource(dict(x=ds_dict["iterations"], top=top, bottom=bottom))
    glyph = bkm.VBar(x="x", top="top", bottom="bottom", width=1, fill_color=colors[4])
    ren = bkm.GlyphRenderer(name="success", data_source=ds_dict["success"], glyph=glyph, visible=True)
    plot.renderers.append(ren)
    litem = bkm.LegendItem(label="Success", renderers=[ren])
    litems.append(litem)

    # Initiate the legend, add it under the plot and load the default data
    legend = bkm.Legend(items=litems, location="center", orientation="horizontal",
                        background_fill_alpha=1.0)
    plot.add_layout(legend, "below")
    select_algo_data(stats_data, algo_ls[0], ds_dict, plot)

    # Add tools to the plot to allow interaction
    node_hover_format = bkm.CustomJSHover(code="return special_vars.name;")
    hover_tl = bkm.HoverTool(show_arrow=False, tooltips=[("Iteration", "@x"), ("Start", "@bottom"),
                                ("End", "@top"), ("Nodes", "@x{custom}")],
                                formatters=dict(x=node_hover_format))
    zoom_tl = bkm.WheelZoomTool()
    plot.toolbar.active_scroll = zoom_tl
    plot.add_tools(hover_tl, bkm.TapTool(), zoom_tl, bkm.PanTool(), bkm.ResetTool(), bkm.SaveTool())

    # Add an algo selection box
    algo_dd = bkm.Select(title="Algorithm: ", value=algo_ls[0], options=algo_ls, height=50,
                            sizing_mode="stretch_width")
    algo_change_callback = bkm.CustomJS(args=dict(stats_data=stats_data, ds_dict=ds_dict,
                                        max_iteration=max_iteration, plot=plot), code=JS_SELECT_ALGO_DATA)
    algo_dd.js_on_change('value', algo_change_callback)

    # Configure the output file attributes, built the page layout and show the graph
    page_layout = bkl.column(children=[plot, algo_dd], sizing_mode="scale_both", margin=(5,20,5,5))
    return page_layout


if __name__ == "__main__":
    # Initiate the argument parser and validate arguments
    parser = ArgumentParser("Helix TE Opti Req")
    parser.add_argument("--data", required=True, type=str, help="YATES Helix TE opti req file")
    args = parser.parse_args()

    if not os.path.exists(args.data) or not os.path.isfile(args.data):
        print("Please enter a valid Helix TE opti request result file")
        exit(1)

    output_file("helix_te_opti_req_graph.html", title="TE Optimisation Request Graph")
    show(generate_page(args.data))
