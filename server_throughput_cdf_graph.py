#!/user/bin/python
import os
from throughput_cdf_graph import generate_page
from base_server import BaseServer


class Graph(BaseServer):
    TITLE = "Throughput CDF Graph"

    DATA_FILES = {
        "Default": ["TotalThroughputVsIterations.dat"],
    }

    def load_data(self, name):
        data_dir = os.path.abspath(os.path.join("DATA", name))
        input = os.path.join(data_dir, "TotalThroughputVsIterations.dat")
        layout = generate_page(input)
        self.update_page_content(layout)


# Generate the page
Graph()
