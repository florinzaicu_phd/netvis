#!/usr/bin/python

# -------------------------------------------------------------
# Script that takes a YATES result file and renames series. The
# script will store the results in a file with name <input_file>REN'
# or a specific output file.
#
# Usage: renameSeries.py --file <file> --rename <rename> --out [out]
#
#   <file> = Path to input file to rename
#
#   <rename> = List of renames seperated by space in syntax:
#              '<search>:<repalce>' where <search is a regex string
#              and <replace> the string we want to replace matching
#              series
#
#   [out] = Path to output renamed file. Defaults to '<file>REN'
#
# -------------------------------------------------------------

from argparse import ArgumentParser

import os
import re
import sys


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--file", required=True, type=str,
        help="Input file to rename series")
    parser.add_argument("--rename", required=True, type=str,
        help="List of renames seperated by spaces with syntax '<search>:<repalce>'.")
    parser.add_argument("--out", required=False, type=str, default=None,
        help="Output file name. Defaults to '<input_file>REN'.")

    # Parse the arguments and make sure the input file is valid and exists
    args = parser.parse_args()
    if not os.path.exists(args.file) or not os.path.isfile(args.file):
        print("Please enter a valid input file")
        sys.exit(1)

    # Generate the output file name and parse the rename string
    out_fp = args.file+"REN"
    if args.out:
        out_fp = args.out

    match = []
    for part in args.rename.split(" "):
        search = part.split(":")
        replace = search[1]
        search = search[0]
        search = re.compile(search)
        match.append((search, replace))

    print("Applying %d renames" % len(match))

    # Open the input file and output file buffers
    with open(args.file, "r") as in_file, open("ren.temp", "w") as out_file:
        for line in in_file:
            if line.startswith("#") or line == "\n":
                # Line is comment or empty, just copy to output
                out_file.write(line)
            else:
                for rename_tup in match:
                    line = rename_tup[0].sub(rename_tup[1], line)
                out_file.write(line)

    # XXX: Use a temporary file to prevent issues when reading and writing to same file
    os.rename("ren.temp", out_fp)
