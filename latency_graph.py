#!/user/bin/python
import sys
import os
import re
from argparse import ArgumentParser

# Visualisation Libraries
from bokeh.io import show, output_file
from bokeh import models as bkm
from bokeh import layouts as bkl
from bokeh import palettes as bkp


# --------------------------------------------------------
# Graph that shows latency (path strech) as a stacked plot
# --------------------------------------------------------


JS_SELECT_ALGO_DATA = """ var algo = cb_obj.value;
var data = stats_data[algo];
plot.title.text = "Path Latency (length) in Hops - " + algo;
var keys = [];
for (var key in data) {
    if (isNaN(key) == false) {
        keys.push(key);
    }
}

for (var i = 0; i < plot.renderers.length; i++) {
    plot.renderers[i].visible = false;

    var key = (i+1).toString();
    var nk = "n" + key;

    for (var q = 0; q <= max_iteration; q++) {
        ds_dict[nk].data["top"][q] = 0;
        ds_dict[nk].data["bottom"][q] = 0;
    }
}

for (var i = 0; i < keys.length; i++) {
    var key = keys[i];
    var nk = "n" + key;

    for (var q = 0; q <= max_iteration; q++) {
        if ((i == 0) || (data[key][q] == 0) || (data[key][q] < data[keys[i-1]][q])) {
            ds_dict[nk].data["bottom"][q] = 0;
        } else {
            var prev = i - 1;
            while ((i > 1) && (prev > 0)) {
                if (data[keys[prev]][q] != 0)
                    break;
                prev --;
            }
            ds_dict[nk].data["bottom"][q] = data[keys[prev]][q];
        }
        ds_dict[nk].data["top"][q] = data[key][q];
    }

    plot.renderers[parseInt(key)-1].visible = true;
    ds_dict[nk].change.emit();
}
"""

def select_algo_data(stats_data, algo, ds_dict, max_iteration, plot):
    """ Show algorithm latency data on the plot. Remove any previous renders and re-create
    them with the new algorithm data.

    Args:
        stats_data (dict): Raw data loaded from the stats result file
        algo (str): Algorithm to show data of
        ds_dict (dict): Dictionary of data sources for the bars shown on the graph
        max_iteration (int): Maximum number of iterations in all result data
        plot (bkm.Plot): Plot to show algorithm data
    """
    data = stats_data[algo]
    plot.title.text = "Path Latency (length) in Hops - %s" % algo

    # Extract keys of node latency to display
    keys = []
    for k in data.keys():
         if isinstance(k, str) and k.isdigit():
            keys.append(k)

    # Hide all renderers
    for ren in plot.renderers:
        ren.visible = False

    # Update the DS with the selected data
    for i in range(len(keys)):
        key = keys[i]
        nk = "n%s" % key

        # Prime the bottom value of the bar as the previous values latency (do not overlap)
        bottom = []
        if i == 0:
            # If this is the first node prime with 0's
            for q in range(max_iteration+1):
                bottom.append(0)
        else:
            bottom = data[keys[i-1]]

        # Update the data and make the renderer visible
        ds_dict[nk].data["top"] = data[key]
        ds_dict[nk].data["bottom"] = bottom
        plot.renderers[int(key)-1].visible = True

def generate_page(input):
    """ Generate and return the page layout showing the data from `input`. Latency
    is displayed as a stacked plot where each value shows the percentage of total
    traffic using a path of length n to reach it's destination.

    Returns:
        obj: Page layout to display
    """
    stats_data = {}
    max_iteration = 0
    with open(input, "r") as fin:
        algo = []
        iter = 0
        # Iterate through the line of the file
        for line in fin:
            line = line.strip()

            # Skip comment or empty lines
            if line == "" or line.startswith("#"):
                continue
            # If this is a result line process it
            elif ":" in line:
                node = int(line.split(":")[0].strip().strip("."))
                u = round(float(line.split(":")[1].strip()) * 100.0, 4)

                if node > stats_data[algo]["max_lat"]:
                    stats_data[algo]["max_lat"] = node
                if node < stats_data[algo]["min_lat"]:
                    stats_data[algo]["min_lat"] = node

                if node not in stats_data[algo]:
                    stats_data[algo][str(node)] = []
                stats_data[algo]["raw"][iter].append((node, u))

            # Otherwise assume it's a algorithm iteration line
            else:
                reg = re.search("([a-zA-Z0-9_]+)\s([0-9]+)", line)
                algo = reg.group(1)
                iter = int(reg.group(2))

                # Prime the dict with default values, if new algo
                if algo not in stats_data:
                    stats_data[algo] = {
                        "raw": {},
                        "max_lat": 0,
                        "min_lat": 100
                    }

                # Check if we found a new max iter and initiate the data list
                if iter > max_iteration:
                    max_iteration = iter

                stats_data[algo]["raw"][iter] = []

    # Iterate through the raw data and process it
    algo_ls = []
    max_lat = 0
    for algo,algo_d in stats_data.items():
        algo_ls.append(algo)
        if algo_d["max_lat"] > max_lat:
            max_lat = algo_d["max_lat"]

        # Prime the value arays with default entries for all iterations
        for i in range(algo_d["min_lat"], algo_d["max_lat"]+1):
            algo_d[str(i)] = []
            for q in range(max_iteration+1):
                algo_d[str(i)].append(0)

        # Process raw data to value arays
        for iter,iter_d in algo_d["raw"].items():
            for n,u in iter_d:
                algo_d[str(n)][iter] = u

        del algo_d["raw"]

    # Initiate the plot and axis
    plot = bkm.Plot(toolbar_location="right", sizing_mode="stretch_both")
    x_axis = bkm.LinearAxis(bounds=(0, max_iteration), axis_label="Iteration Number")
    plot.add_layout(x_axis, 'below')
    y_axis = bkm.LinearAxis(bounds=(0, 100), axis_label="% of Traffic")
    plot.add_layout(y_axis, 'left')
    plot.add_layout(bkm.Grid(dimension=0, ticker=x_axis.ticker))
    plot.add_layout(bkm.Grid(dimension=1, ticker=y_axis.ticker))

    colors = bkp.Category20[20]
    ds_dict = {"iterations": list(range(max_iteration+1))}
    litems = []
    for i in range(1, max_lat+1):
        node_key = "n%d" % i
        top = []
        bottom = []
        for q in range(max_iteration+1):
            top.append(0)
            bottom.append(0)
        ds_dict[node_key] = bkm.ColumnDataSource(dict(x=ds_dict["iterations"], top=top, bottom=bottom))

        # XXX: The category limit is 20 items, if we have paths that have more
        # nodes, wrap the index to prevent errors. 20-items is the max category
        # size.
        color_index = (i-1) % 20

        glyph = bkm.VBar(x="x", top="top", bottom="bottom", width=1, fill_color=colors[color_index])
        ren = bkm.GlyphRenderer(name=node_key, data_source=ds_dict[node_key], glyph=glyph, visible=False)
        plot.renderers.append(ren)

        # Add a legend item for the node bar
        litem = bkm.LegendItem(label="Node %d" % i, renderers=[ren])
        litems.append(litem)

    # Create the legend and add it under the plot
    legend = bkm.Legend(items=litems, location="center", orientation="horizontal",
                        background_fill_alpha=1.0)
    plot.add_layout(legend, "below")

    # List of iterations for the latency data
    select_algo_data(stats_data, algo_ls[0], ds_dict, max_iteration, plot)

    # Add tools to the plot to allow interaction
    node_hover_format = bkm.CustomJSHover(code="""var name = special_vars.name;
        name = name.substr(1);
        return name;
    """)
    hover_tl = bkm.HoverTool(show_arrow=False, tooltips=[("Iteration", "@x"), ("Start", "@bottom"),
                                ("End", "@top"), ("Nodes", "@x{custom}")],
                                formatters=dict(x=node_hover_format))
    zoom_tl = bkm.WheelZoomTool()
    plot.toolbar.active_scroll = zoom_tl
    plot.add_tools(hover_tl, bkm.TapTool(), zoom_tl, bkm.PanTool(), bkm.ResetTool(), bkm.SaveTool())

    # Add an algo selection box
    algo_dd = bkm.Select(title="Algorithm: ", value=algo_ls[0], options=algo_ls, height=50,
                            sizing_mode="stretch_width")
    algo_change_callback = bkm.CustomJS(args=dict(stats_data=stats_data, ds_dict=ds_dict,
                                        max_iteration=max_iteration, plot=plot), code=JS_SELECT_ALGO_DATA)
    algo_dd.js_on_change('value', algo_change_callback)

    # Configure the output file attributes, built the page layout and show the graph
    page_layout = bkl.column(children=[plot, algo_dd], sizing_mode="scale_both", margin=(5,20,5,5))
    return page_layout

if __name__ == "__main__":
    # Initiate the argument parser and validate arguments
    parser = ArgumentParser("Latency Graph")
    parser.add_argument("--data", required=True, type=str, help="YATES latency file")
    args = parser.parse_args()

    if not os.path.exists(args.data) or not os.path.isfile(args.data):
        print("Please enter a valid latency file")
        exit(1)

    output_file("latency_graph.html", title="Latency Graph")
    show(generate_page(args.data))
