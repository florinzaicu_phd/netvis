#!/user/bin/python
import sys
import os
import re
import json
from argparse import ArgumentParser

# Visualisation Libraries
from bokeh.io import show, output_file
from bokeh import models as bkm
from bokeh import layouts as bkl
from bokeh import palettes as bkp

# Import helper methods
from graph_helper import init_nx, init_plot, gen_plot_style_controls


# --------------------------------------------------------
# Graph that shows bidirectional link usage of a topology
# --------------------------------------------------------


# JS code that is used to show the node and host information on selection of a node or
# update of the utilisation DS
JS_node_select ="""var node_data = n_ds.data;
var ind = n_ds.selected["1d"]["indices"][0];
var u_ds = u_ds.data;
div = document.getElementById("embed");

if (ind == null) {
    div.innerHTML = "Click on node to view edge info!";
    return;
}

var node_country = node_data["Country"][ind];
var node_label = node_data["label"][ind];
var node_host = "N/A";
if ("Host" in node_data)
    node_host = node_data["Host"][ind];
var node_ufrm_host = node_data["usage_from_host"][ind].toFixed(4);
var node_uto_host = node_data["usage_to_host"][ind].toFixed(4);

div.innerHTML = "<strong>Selected Node Info</strong><br />";
div.innerHTML += "<em>ID:</em> "+ind+"<br />";
div.innerHTML += "<em>Label:</em> "+node_label+"<br />";
div.innerHTML += "<em>Country:</em> "+node_country+"<br />";
div.innerHTML += "<em>Host:</em> "+node_host+"<br />";
div.innerHTML += "<em>Usage From Host:</em> "+node_ufrm_host+"<br />";
div.innerHTML += "<em>Usage To Host:</em> "+node_uto_host+"<br />";

div.innerHTML += "<br /><strong>Selected Node Edges Info</strong><br />";

for (var i = 0; i < u_ds["from"].length; i++) {
    if (u_ds["from"][i] == node_data["index"][ind]) {
        div.innerHTML += "<hr /><strong>Link Info</strong><br />";
        div.innerHTML += "<em>ID:</em> "+i+"<br />";
        div.innerHTML += "<em>From:</em> "+u_ds["from"][i]+"<br />";
        div.innerHTML += "<em>To:</em> "+u_ds["to"][i]+"<br />";
        div.innerHTML += "<em>Usage:</em> "+u_ds["usage"][i].toFixed(4)+"<br />";
    }
}
"""

# JS code which is the equivalent of the python select_usage_data method
JS_select_usage_data = ("""algo = algo.value;
if (iter != null) {
    iter = iter.value;
} else {
    for (key in d[algo]) {
        iter = key
        break;
    }
}
var data = d[algo][iter];
plot.title.text = "Edge Congestion Plot - Algorithm " + algo + " iteration " + iter;

for (key in data) {
    var kfrom = key.split("-")[0];
    var kto = (key.split("-"))[1];
    var kfrom_i = kfrom
    var kto_i = kto

    if (key.indexOf("h") > -1) {
        var target = -1;
        if (kfrom.indexOf("h") > -1) {
            kfrom_i = -1;
            target = kto_i;
        }
        if (kto.indexOf("h") > -1) {
            kto_i = -1;
            target = kfrom_i;
        }

        for (var i = 0; i < n_ds.data["Host"].length; i++) {
            if (n_ds.data["index"][i] == target) {
                if ((kfrom != n_ds.data["Host"][i]) && (kto != n_ds.data["Host"][i])) {
                    console.log("Error: Found node but host dosen't match!");
                    continue;
                }

                if (kfrom_i == -1)
                    n_ds.data["usage_from_host"][i] = data[key];
                if (kto_i == -1)
                    n_ds.data["usage_to_host"][i] = data[key];
            }
        }
        continue;
    }

    for (var i = 0; i < u_ds.data["to"].length; i++) {
        if ((u_ds.data["from"][i] == kfrom_i) && (u_ds.data["to"][i] == kto_i)) {
            u_ds.data["usage"][i] = data[key];
            break;
        }
    }
}

for (var i = 0; i < e_ds.data["start"].length; i++) {
    e_ds.data["color"][i] = id_info["edge_default"]["color"];
}
for (var i = 0; i < n_ds.data["id"].length; i++) {
    n_ds.data["color"][i] = id_info["node_default"]["color"];
}

if (algo.includes("HelixMC")) {
    for (var i = 0; i < e_ds.data["start"].length; i++) {
        test_idp = e_ds.data["start"][i] + "-" + e_ds.data["end"][i];
        if (id_info["link"].includes(test_idp)) {
            e_ds.data["color"][i] = "#ff0000";
        }
    }

    for (var i = 0; i < n_ds.data["id"].length; i++) {
        test_node = n_ds.data["id"][i];
        if (test_node in id_info["node"]) {
            n_ds.data["color"][i] = id_info["node"][test_node];
        }
    }
}

u_ds.change.emit();
n_ds.change.emit();
e_ds.change.emit();
"""
+ JS_node_select)

def select_usage_data(stats_data, util_ds, node_ds, edge_ds, id_info, algo, iter):
    """ Changes the usage data currently being shown, triggering a UI update.
    The method will go through the raw stats results, updating the usage in the
    util datasource `util_ds` by showing the specified algorithm `algo` and iteration
    `iter` results.

    Args:
        stats_data (dict): Raw data loaded from the stats results file
        util_ds (ColumnDataSource): Utilisation datasource that shows link usage
        node_ds (ColumnDataSource): Node datasource which contains all node info
        edge_ds (ColumnDataSource): Display edge datasource to show IDP links for HelixMC
        id_info (dict): Inter-domain information to show for HelixMC
        algo (string): Algorithm to load data for
        iter (int): Iteration of algorithm data to show
    """
    # Retrieve the raw data dictionary to process and go through link keys
    data = stats_data[algo][iter]
    for key,usage in data.items():
        # Extract the node labels from the key and indexes
        kfrom = key.split("-")[0]
        kto = key.split("-")[1]
        kfrom_i = kfrom
        kto_i = kto

        # If the node is a host do special processing
        if "h" in key:
            target = -1
            if "h" in kfrom:
                kfrom_i = -1
                target = kto_i
            if "h" in kto:
                kto_i = -1
                target = kfrom_i

            # Iterate through nodes and update host usage
            for i in range(len(node_ds.data["Host"])):
                if node_ds.data["index"][i] == target:
                    if not kfrom == node_ds.data["Host"][i] and not kto == node_ds.data["Host"][i]:
                        print("Found node but host dosen't match!")
                        continue

                    if kfrom_i == -1:
                        node_ds.data["usage_from_host"][i] = usage
                    if kto_i == -1:
                        node_ds.data["usage_to_host"][i] = usage
                    break
            continue

        # Change the node usage in the usage datasource
        for i in range(len(util_ds.data["to"])):
            if str(util_ds.data["from"][i]) == kfrom_i and str(util_ds.data["to"][i]) == kto_i:
                util_ds.data["usage"][i] = usage
                break

    # Reset the color of the edges and nodes
    for i in range(len(edge_ds.data["start"])):
        edge_ds.data["color"][i] = id_info["edge_default"]["color"]
    for i in range(len(node_ds.data["id"])):
        node_ds.data["color"][i] = id_info["node_default"]["color"]

    # Highlight inter-domain nodes and links if algorithm is HelixMC
    if "HelixMC" in algo:
        # Update the color of the inter-domain edges
        for i in range(len(edge_ds.data["start"])):
            test_idp = "%s-%s" % (edge_ds.data["start"][i], edge_ds.data["end"][i])
            if test_idp in id_info["link"]:
                edge_ds.data["color"][i] = "#ff0000"

        # Color the nodes for different domains
        for i in range(len(node_ds.data["id"])):
            test_node = node_ds.data["id"][i]
            if test_node in id_info["node"]:
                node_ds.data["color"][i] = id_info["node"][test_node]

def generate_page(topo_input, edge_input, node_assoc=None, sw_ctrl_map=None, host_name=None):
    """ Generate and return the page layout showing the data for topology `topo_input`
    and data `edge_input`. The graph shows the bidirectional link usage of a topology.

    Args:
        topo_input (str): Path to the topology file used to collect results
        edge_input (str): Path to edge congestion YATES output file
        node_assoc (str): Optional path to JSON file that associations result
            node name to topo node ID.
        sw_ctrl_map (str): Optional switch to controller map file to show
            domains on topology
        host_name (str): Optional path to JSON file that describes host
            name of labels (prevents auto generation with node IDs).

    Returns:
        obj: Page layout to display
    """
    # Load the host name data from the provided file
    host_names  = None
    if host_name is not None:
        with open(host_name, "r") as fin:
            host_names = json.loads(fin.read())

    # Load the topology data and initiate the utilisation DS
    util_ds = {
        "from": [],
        "to": [],
        "usage": [],
        "xs": [],
        "ys": []
    }
    g, pos, util_ds = init_nx(topo_input, util_ds, node_extra={"usage_from_host": 0,
                                "usage_to_host": 0},
                                edge_init=lambda d: d["usage"].append(0.0),
                                host_names=host_names)

    # Load the association data from the provided file
    assoc_data = {}
    if node_assoc is not None:
        with open(node_assoc, "r") as fin:
            assoc_data = json.loads(fin.read())

    stats_data = {}
    with open(edge_input, "r") as fin:
        algo = []
        iter = 0
        for line in fin:
            line = line.strip()
            # Check if the line is a comment or empty
            if line == "" or line.startswith("#"):
                continue
            # Check if this is result line
            elif ":" in line:
                # Split the tokens in the data line
                key_raw = line.split(":")[0].strip()
                reg = re.search("\(([a-zA-Z0-9-]+),([a-zA-Z0-9-]+)\)", key_raw)
                kfrom = reg.group(1)
                kto = reg.group(2)

                if kfrom in assoc_data:
                    kfrom = assoc_data[kfrom]
                if kto in assoc_data:
                    kto = assoc_data[kto]
                key = "%s-%s" % (kfrom, kto)

                u = round(float(line.split(":")[1].strip()), 4)
                stats_data[algo][iter][key] = u
            # Otherwise assume it's a algorithm iteration line
            else:
                # Process the algo and iter
                reg = re.search("([a-zA-Z0-9_]+)\s([0-9]+)", line)
                algo = reg.group(1)
                iter = int(reg.group(2))

                if algo not in stats_data:
                    stats_data[algo] = {}
                if iter not in stats_data[algo]:
                    stats_data[algo][iter] = {}

    # -------------------- GENERATE GRAPH -------------------


    plot, graph_ren = init_plot("Edge Congestion Plot", g, pos, port_ind_ds=util_ds,
                                port_ind_field="usage", port_ind_scale=(0.00, 1.00),
                                port_ind_title="Usage")
    node_ren = graph_ren.node_renderer
    edge_ren = graph_ren.edge_renderer
    node_ds = node_ren.data_source
    edge_ds = edge_ren.data_source

    # Custom callback on node selection
    callback_node = bkm.CustomJS(args=dict(n_ds=node_ds, u_ds=util_ds), code=JS_node_select)
    node_ds.selected.js_on_change('indices', callback_node)

    # Go through and process the idp links information
    inter_dom_info = {"link": [], "node": {},
        "edge_default": {"color": "#cccccc", "sel_color": bkp.Spectral4[2]},
        "node_default": {"color": bkp.Spectral4[0], "sel_color": bkp.Spectral4[2]}
    }
    domains = []
    if sw_ctrl_map is not None:
        with open(sw_ctrl_map, "r") as fin:
            tmp = json.loads(fin.read())
            for ctrl,ctrl_d in tmp["ctrl"].items():
                # Process the switches of the domain
                for sw in ctrl_d["sw"]:
                    if ctrl not in domains:
                        domains.append(ctrl)
                    i = domains.index(ctrl)
                    if sw in assoc_data:
                        sw = assoc_data[sw]
                    inter_dom_info["node"][sw] = bkp.Pastel1[9][i]

                # Process the inter-domain links
                for dom,dom_d in ctrl_d["dom"].items():
                    for info in dom_d:
                        info_from = info["sw"]
                        info_to = info["sw_to"]
                        if info_from in assoc_data:
                            info_from = assoc_data[info_from]
                        if info_to in assoc_data:
                            info_to = assoc_data[info_to]

                        idp = "%s-%s" % (info_from, info_to)
                        if idp not in inter_dom_info["link"]:
                            inter_dom_info["link"].append(idp)


    # -------------------- DISPLAY CONTROLS ---------------------


    # Create contain to show info of clicked nodes and connected links
    embed_div = bkm.Div(text="<div id='embed'>Click on node to view edge info!</div>",
        style={"padding": "20px", "width": "80%", "background": "#eee", "box-sizing":
                "border-box"})

    # Create controls to allow changing algo and iter being shown
    algo_ls = list(stats_data.keys())
    start = algo_ls[0]
    algo_dd = bkm.Select(title="Algorithm: ", value=start, options=algo_ls, height=50,
                            sizing_mode="stretch_width")

    iter_ls = list(stats_data[start].keys())
    iter_ls.sort()
    start = int(iter_ls[0])
    end = int(iter_ls[len(iter_ls)-1])
    iter_sl = None
    if not start == end:
        iter_sl = bkm.Slider(start=start, end=end, step=1, value=start, title="Iteration",
                            height=50, sizing_mode="stretch_width")

    # Prime with default data and bind callback to albo and iter slides to change data shown
    select_usage_data(stats_data, util_ds, node_ds, edge_ds, inter_dom_info, algo_dd.value, start)
    ui_change_callback = bkm.CustomJS(args=dict(d=stats_data, u_ds=util_ds, n_ds=node_ds,
                                        e_ds=edge_ds, id_info=inter_dom_info,
                                        algo=algo_dd, iter=iter_sl, plot=plot),
                                        code=JS_select_usage_data)
    algo_dd.js_on_change('value', ui_change_callback)
    if iter_sl is not None:
        iter_sl.js_on_change('value', ui_change_callback)

    # Update the plot title
    plot.title.text = "Edge Congestion Plot - Algorithm %s iteration %s" % (algo_dd.value, start)


    # -------------------- CHART UI AND SHOW GRAPH ---------------------


    # Configure the output file attributes, built the page layout and show the graph
    opt_layout = bkl.column(children=gen_plot_style_controls(plot), width=120)
    main_layout = bkl.row(children=[opt_layout, bkl.Spacer(width=10), plot])

    # If the iteration slider should not be displayed do not add it to page
    ctrl_elements = [main_layout, algo_dd]
    if iter_sl is not None:
        ctrl_elements.append(iter_sl)
    ctrl_elements.append(embed_div)

    page_layout = bkl.column(children=ctrl_elements, sizing_mode="scale_both",
                                margin=(5,20,5,5))
    return page_layout

if __name__ == "__main__":
    # Initiate the argument parser and validate arguments
    parser = ArgumentParser("Edge Congestion Graph")
    parser.add_argument("--topo", required=True, type=str, help="Topology file (.dot or .gml)")
    parser.add_argument("--data", required=True, type=str, help="YATES edge congestion file")
    parser.add_argument("--node_assoc", required=False, type=str, default=None,
            help="Node association JSON file (optional)")
    parser.add_argument("--sw_ctrl_map", required=False, type=str, default=None,
            help="HelixMC switch to controller map file (optional)")
    args = parser.parse_args()

    if not os.path.exists(args.topo) or not os.path.isfile(args.topo):
        print("Please enter a valid topology file")
        exit(1)

    if not os.path.exists(args.data) or not os.path.isfile(args.data):
        print("Please enter a valid edge congestion file")
        exit(1)

    # If the node assoc file is invalid do not load it
    if (args.node_assoc is None or not os.path.exists(args.node_assoc)
                                        or not os.path.isfile(args.node_assoc)):
        args.node_assoc = None

    # If the switch to controller map argument is invalid do not load it
    if (args.sw_ctrl_map is None or not os.path.exists(args.sw_ctrl_map)
                                        or not os.path.isfile(args.sw_ctrl_map)):
        args.sw_ctrl_map = None

    output_file("edge_con_graph.html", title="Edge Congestion Graph")
    show(generate_page(args.topo, args.data, args.node_assoc, args.sw_ctrl_map))
