#!/user/bin/python
import sys
import os
import re
import math
from argparse import ArgumentParser

# Visualisation Libraries
from bokeh.io import show, output_file
from bokeh import models as bkm
from bokeh import layouts as bkl
from bokeh import palettes as bkp
from congestion_graph import process_file_keywords


# --------------------------------------------------------
# Graph that shows congestion CDF data
# --------------------------------------------------------


def generate_page(input):
    """ Generate and return the page layout showing data for `input`. This script can
    be used to plot max, mean, max exponential, mean exponential or congestion loss
    CDF (all congestion files). The title and axis are controllered by the `input`.
    See ``process_file_keywords`` for keywords.

    CDF is computed for the entire iteration data-set by ordering the values and applying
    a cumulative PDF of 1/N to every congestion value.

    Returns:
        obj: Page layout to display.
    """
    title_prefix, is_congestion_loss = process_file_keywords(input)

    stats_data = {}
    max_iteration = 0
    max_value = 0
    algo_ls = []

    # Read and process the raw file data
    with open(input, "r") as fin:
        for line in fin:
            line = line.strip()

            # Skip comment or empty lines
            if line == "" or line.startswith("#"):
                continue

            # Extract the data fields
            reg = re.search("([a-zA-Z0-9_]+)\s+([0-9]+)\s+([0-9.]+)\s+([0-9.]+)", line)
            algo = reg.group(1)
            iter = int(reg.group(2)) + 1
            value = float(reg.group(3)) * 100

            if algo not in stats_data:
                stats_data[algo] = {"cdf": [], "value": []}
                algo_ls.append(algo)

            stats_data[algo]["value"].append(value)
            if max_iteration < iter:
                max_iteration = iter
            if max_value < value:
                max_value = value

    # Order the data and compute the CDF for the dataset
    for algo,algo_d in stats_data.items():
        algo_d["value"].sort()
        for i in range(max_iteration):
            pdf = 1/max_iteration
            if i == 0:
                algo_d["cdf"].append(pdf)
            else:
                algo_d["cdf"].append(pdf + algo_d["cdf"][i-1])

    # Round max value to nearest 10 and initate the plot and axis
    max_value = int(math.ceil(max_value / 10)) * 10
    plot = bkm.Plot(toolbar_location="right", sizing_mode="stretch_both")

    if is_congestion_loss:
        plot.title.text = "%s Congestion Loss CDF" % title_prefix
        x_axis = bkm.LinearAxis(bounds=(0, max_value), axis_label="Loss %")
        tooltips = [("Loss %", "@x"), ("F(x)", "@y"), ("Algorithm", "@name")]
    else:
        plot.title.text = "%s Link Usage (Congestion) CDF" % title_prefix
        x_axis = bkm.LinearAxis(bounds=(0, max_value), axis_label="Usage %")
        tooltips = [("Usage %", "@x"), ("F(x)", "@y"), ("Algorithm", "@name")]

    plot.add_layout(x_axis, 'below')
    y_axis = bkm.LinearAxis(bounds=(0, 1), axis_label="F(x)")
    plot.add_layout(y_axis, 'left')
    plot.add_layout(bkm.Grid(dimension=0, ticker=x_axis.ticker))
    plot.add_layout(bkm.Grid(dimension=1, ticker=y_axis.ticker))

    # Initiate the plot lines and legend elements
    legend_items = []
    colors = bkp.Category10[10]
    colors.extend(bkp.Pastel1[8])
    for i in range(len(algo_ls)):
        algo = algo_ls[i]
        algo_d = stats_data[algo]
        ds_dict = {
                "x": algo_d["value"],
                "y": algo_d["cdf"],
                "name": ([algo] * len(algo_d["cdf"]))
        }
        ds = bkm.ColumnDataSource(ds_dict)
        glyph = bkm.Line(x="x", y="y", line_width=2, line_color=colors[i])
        ren = bkm.GlyphRenderer(name=algo, data_source=ds, glyph=glyph, visible=True)
        plot.renderers.append(ren)

        #glyph_pt = bkm.Cross(x="x", y="y", line_width=6, line_color=colors[i])
        #ren_pt = bkm.GlyphRenderer(name="%s_pt" % algo, data_source=ds, glyph=glyph_pt, visible=False)
        #plot.renderers.append(ren_pt)

        legend_item = bkm.LegendItem(label=algo, renderers=[ren]) #, ren_pt])
        legend_items.append(legend_item)

    # Create the legend and add it under the plot
    legend = bkm.Legend(items=legend_items, location="center", orientation="horizontal",
                        background_fill_alpha=1.0, click_policy="hide")
    plot.add_layout(legend, "below")

    # Add tools to the plot to allow interaction
    hover_tl = bkm.HoverTool(show_arrow=False, tooltips=tooltips)
    zoom_tl = bkm.WheelZoomTool()
    plot.toolbar.active_scroll = zoom_tl
    plot.add_tools(hover_tl, bkm.TapTool(), zoom_tl, bkm.PanTool(), bkm.ResetTool(), bkm.SaveTool())

    # Configure the output file attributes, built the page layout and show the graph
    page_layout = bkl.column(children=[plot], sizing_mode="scale_both", margin=(5,20,5,5))
    return page_layout

if __name__ == "__main__":
    # Initiate the argument parser and validate arguments
    parser = ArgumentParser("Congestion CDF Graph")
    parser.add_argument("--data", required=True, type=str, help="YATES congestion file")
    args = parser.parse_args()

    if not os.path.exists(args.data) or not os.path.isfile(args.data):
        print("Please enter a valid congestion file")
        exit(1)

    title_prefix, is_congestion_loss = process_file_keywords(args.data)
    if is_congestion_loss:
        title = "%s Congestion Loss CDF Graph" % title_prefix
    else:
        title = "%s - Congestion CDF Graph" % title_prefix

    output_file("congestion_cdf_graph.html", title=title)
    show(generate_page(args.data))
