#!/user/bin/python
import sys
import os
import re
import math
from argparse import ArgumentParser

# Visualisation Libraries
from bokeh.io import show, output_file
from bokeh import models as bkm
from bokeh import layouts as bkl
from bokeh import palettes as bkp
from graph_helper import order_insert


# --------------------------------------------------------
# Graph that shows total throughput data files
# --------------------------------------------------------


def generate_page(input):
    """ Generate and return the page layout showing data for `input`. This script can
    be used to plot throughput data.

    Returns:
        obj: Page layout to display.
    """
    stats_data = {}
    max_iteration = 0
    algo_ls = []

    # Read and process the raw file data
    with open(input, "r") as fin:
        for line in fin:
            line = line.strip()

            # Skip comment or empty lines
            if line == "" or line.startswith("#"):
                continue

            # Extract the data fields
            reg = re.search("([a-zA-Z0-9_]+)\s+([0-9]+)\s+([0-9.]+)\s+([0-9.]+)", line)
            algo = reg.group(1)
            iter = int(reg.group(2)) + 1
            value = float(reg.group(3)) * 100

            if algo not in stats_data:
                stats_data[algo] = {"value": [], "iter": []}
                algo_ls.append(algo)

            # Order insert the data based on the iteration number
            insert_index = order_insert(iter, stats_data[algo]["iter"])
            if insert_index == -1:
                stats_data[algo]["value"].append(value)
            else:
                stats_data[algo]["value"].insert(insert_index, value)

            if max_iteration < iter:
                max_iteration = iter

    # Round the max value to the nearest 10 and initiate plot and axis
    plot = bkm.Plot(toolbar_location="right", sizing_mode="stretch_both")
    plot.title.text = "Throughput per Iteration"
    y_axis = bkm.LinearAxis(bounds=(0, 100), axis_label="% Traffic")
    tooltips = [("Iteration", "@x"), ("Traffic %", "@y"), ("Algorithm", "@name")]

    x_axis = bkm.LinearAxis(bounds=(0, max_iteration), axis_label="Iteration Number")
    plot.add_layout(x_axis, 'below')
    plot.add_layout(y_axis, 'left')
    plot.add_layout(bkm.Grid(dimension=0, ticker=x_axis.ticker))
    plot.add_layout(bkm.Grid(dimension=1, ticker=y_axis.ticker))

    # Initiate the plot lines and legend elements
    legend_items = []
    colors = bkp.Category10[10]
    colors.extend(bkp.Pastel1[8])
    for i in range(len(algo_ls)):
        algo = algo_ls[i]
        algo_d = stats_data[algo]
        ds_dict = {
                "x": algo_d["iter"],
                "y": algo_d["value"],
                "name": ([algo] * len(algo_d["iter"]))
        }
        ds = bkm.ColumnDataSource(ds_dict)
        glyph = bkm.Line(x="x", y="y", line_width=2, line_color=colors[i])
        ren = bkm.GlyphRenderer(name=algo, data_source=ds, glyph=glyph, visible=True)
        plot.renderers.append(ren)

        glyph_pt = bkm.Cross(x="x", y="y", line_width=6, line_color=colors[i])
        ren_pt = bkm.GlyphRenderer(name="%s_pt" % algo, data_source=ds, glyph=glyph_pt, visible=True)
        plot.renderers.append(ren_pt)

        legend_item = bkm.LegendItem(label=algo, renderers=[ren, ren_pt])
        legend_items.append(legend_item)

    # Create the legend and add it under the plot
    legend = bkm.Legend(items=legend_items, location="center", orientation="horizontal",
                        background_fill_alpha=1.0, click_policy="hide")
    plot.add_layout(legend, "below")

    hover_tl = bkm.HoverTool(show_arrow=False, tooltips=tooltips)
    zoom_tl = bkm.WheelZoomTool()
    plot.toolbar.active_scroll = zoom_tl
    plot.add_tools(hover_tl, bkm.TapTool(), zoom_tl, bkm.PanTool(), bkm.ResetTool(), bkm.SaveTool())

    # Configure the output file attributes, built the page layout and show the graph
    page_layout = bkl.column(children=[plot], sizing_mode="scale_both", margin=(5,20,5,5))
    return page_layout

if __name__ == "__main__":
    # Initiate the argument parser and validate arguments
    parser = ArgumentParser("Throughput Graph")
    parser.add_argument("--data", required=True, type=str, help="YATES throughput file")
    args = parser.parse_args()

    if not os.path.exists(args.data) or not os.path.isfile(args.data):
        print("Please enter a valid throughput file")
        exit(1)

    output_file("throughput_graph.html", title="Throughput Graph")
    show(generate_page(args.data))
