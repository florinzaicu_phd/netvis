#!/user/bin/python
import os
from helix_te_opti_req_graph import generate_page
from base_server import BaseServer


class Graph(BaseServer):
    TITLE = "Helix TE Optimisation Request Graph"
    DATA_FILES = {"Default": ["HelixOptiTECountVsIterations.dat"]}

    def load_data(self, name):
        data_dir = os.path.abspath(os.path.join("DATA", name))
        input = os.path.join(data_dir, "HelixOptiTECountVsIterations.dat")
        layout = generate_page(input)
        self.update_page_content(layout)


# Generate the page
Graph()
