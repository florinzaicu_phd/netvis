#!/user/bin/python
import sys
import os
import re
import json
import math
from argparse import ArgumentParser

# Visualisation Libraries
from bokeh.io import show, output_file
from bokeh import models as bkm
from bokeh import layouts as bkl
from bokeh import palettes as bkp

# Import helper methods
from graph_helper import init_nx, init_plot, gen_plot_style_controls, get_plot_port_ind


# --------------------------------------------------------
# Graph that shows port usage by the Helix path computation algorithm
# --------------------------------------------------------


# JS code that is used to show the node and host information on selection of a node
JS_node_select ="""var node_data = n_ds.data;
var ind = n_ds.selected["1d"]["indices"][0];
var i_ds = i_ds.data;
div = document.getElementById("embed");

if (ind == null) {
    div.innerHTML = "Click on node to view edge info!";
    return;
}

var node_country = node_data["Country"][ind];
var node_label = node_data["label"][ind];
var node_host = "N/A";
if ("Host" in node_data)
    node_host = node_data["Host"][ind];

div.innerHTML = "<strong>Selected Node Info</strong><br />";
div.innerHTML += "<em>ID:</em> "+ind+"<br />";
div.innerHTML += "<em>Label:</em> "+node_label+"<br />";
div.innerHTML += "<em>Country:</em> "+node_country+"<br />";
div.innerHTML += "<em>Host:</em> "+node_host+"<br />";

div.innerHTML += "<br /><strong>Selected Node Edges Info</strong><br />";

for (var i = 0; i < i_ds["from"].length; i++) {
    if (i_ds["from"][i] == node_data["index"][ind]) {
        div.innerHTML += "<hr /><strong>Link Info</strong><br />";
        div.innerHTML += "<em>ID:</em> "+i+"<br />";
        div.innerHTML += "<em>From:</em> "+i_ds["from"][i]+"<br />";
        div.innerHTML += "<em>To:</em> "+i_ds["to"][i]+"<br />";
        div.innerHTML += "<em>Used by Primary:</em> "+i_ds["u_pri"][i]+"<br />";
        div.innerHTML += "<em>Used by Secondary:</em> "+i_ds["u_sec"][i]+"<br />";
        div.innerHTML += "<em>Used by Splice:</em> "+i_ds["u_splice"][i]+"<br />";
        div.innerHTML += "<em>Total Usage:</em> "+i_ds["u_total"][i]+"<br />";
    }
}
"""

# JS code that is used to select a path type to show usage count of links on graph
JS_select_path_type ="""var val = cb_obj.value;
if (val == "Primary") {
    ui.glyph.line_color.field="u_pri";
    plot.title.text = "Helix Path Link Used - Primary Path";
} else if (val == "Secondary") {
    ui.glyph.line_color.field="u_sec";
    plot.title.text = "Helix Path Link Used - Secondary Path";
} else if (val == "Splice") {
    ui.glyph.line_color.field="u_splice";
    plot.title.text = "Helix Path Link Used - Splice Path";
} else {
    ui.glyph.line_color.field="u_total";
    plot.title.text = "Helix Path Link Used - All Paths";
}
ui.glyph.change.emit();"""

# JS code to change if we are showing domains on topology
JS_select_display_dom = """var val = cb_obj.value;

for (var i = 0; i < e_ds.data["start"].length; i++) {
    e_ds.data["color"][i] = id_info["edge_default"]["color"];
}
for (var i = 0; i < n_ds.data["id"].length; i++) {
    n_ds.data["color"][i] = id_info["node_default"]["color"];
}

if (val == "Yes") {
    for (var i = 0; i < e_ds.data["start"].length; i++) {
        test_idp = e_ds.data["start"][i] + "-" + e_ds.data["end"][i];
        if (id_info["link"].includes(test_idp)) {
            e_ds.data["color"][i] = "#ff0000";
        }
    }

    for (var i = 0; i < n_ds.data["id"].length; i++) {
        test_node = n_ds.data["id"][i];
        if (test_node in id_info["node"]) {
            n_ds.data["color"][i] = id_info["node"][test_node];
        }
    }
}

n_ds.change.emit();
e_ds.change.emit();
"""

def inc_edge_used(path, target_key, target_ds, assoc_data):
    """ Incremenent the number of usaged for a particular edge (port) """
    for i in range(len(path)-1):
        p1 = path[i]
        p2 = path[i+1]

        if p1 in assoc_data:
            p1 = assoc_data[p1]
        if p2 in assoc_data:
            p2 = assoc_data[p2]

        for q in range(len(target_ds["to"])):
            if target_ds["from"][q] == p1 and target_ds["to"][q] == p2:
                target_ds[target_key][q] += 1
                break

def generate_page(topo_input, path_input, node_assoc=None, sw_ctrl_map=None, host_name=None):
    """ Generate and return page layout showing data from `path_input` on topology
    `topo_input`. The graph shows the number of times each port of the topology is
    used by the computed helix paths.

    Args:
        topo_input (str): Path to the topology file used to collect results
        path_input (str): Path to the helix computed paths file
        node_assoc (str): Optional path to JSON file that associations result
            node name to topo node ID.
        sw_ctrl_map (str): Optional switch to controller map file to show
            domains on topology
        host_name (str): Optional path to JSON file that describes host
            name of labels (prevents auto generation with node IDs).

    Returns:
        obj: Page layout to display
    """
    # Load the host name data from the provided file
    host_names  = None
    if host_name is not None:
        with open(host_name, "r") as fin:
            host_names = json.loads(fin.read())

    # Load the topology data and initiate the info DS
    info_ds = {
        "from": [],
        "to": [],
        "u_pri": [],
        "u_sec": [],
        "u_splice": [],
        "u_total": [],
        "xs": [],
        "ys": []
    }
    g, pos, info_ds = init_nx(topo_input, info_ds, edge_init=lambda d: (
                                d["u_pri"].append(0), d["u_sec"].append(0),
                                d["u_splice"].append(0), d["u_total"].append(0)
                            ), host_names=host_names)

    # Parse the path JSON file and compute how many times each node is used
    with open(path_input, "r") as fin:
        path_data = json.loads(fin.read())

    assoc_data = {}
    if node_assoc is not None:
        with open(node_assoc, "r") as fin:
            assoc_data = json.loads(fin.read())

    for hfrom, hfrom_d in path_data.items():
        for hto, hto_d in hfrom_d.items():
            inc_edge_used(hto_d["primary"], "u_pri", info_ds.data, assoc_data)
            inc_edge_used(hto_d["secondary"], "u_sec", info_ds.data, assoc_data)
            for path in hto_d["splice"]:
                inc_edge_used(path, "u_splice", info_ds.data, assoc_data)

    # Generate total usage and work out MAX paths for graph scale
    max_paths = 0
    for i in range(len(info_ds.data["u_pri"])):
        info_ds.data["u_total"][i] += info_ds.data["u_pri"][i]
        info_ds.data["u_total"][i] += info_ds.data["u_sec"][i]
        info_ds.data["u_total"][i] += info_ds.data["u_splice"][i]

        if info_ds.data["u_total"][i] > max_paths:
            max_paths = info_ds.data["u_total"][i]

    # Make max paths a multiple of 20
    max_paths = int(math.ceil(max_paths / 20.0)) * 20

    # Generate the plot and renderers
    plot, graph_ren = init_plot("Helix Path Link Used - Primary Path", g, pos, port_ind_ds=info_ds,
                                port_ind_field="u_pri", port_ind_scale=(0, max_paths),
                                port_ind_title="# Paths")
    node_ren = graph_ren.node_renderer
    edge_ren = graph_ren.edge_renderer
    node_ds = node_ren.data_source
    edge_ds = edge_ren.data_source

    callback_node = bkm.CustomJS(args=dict(n_ds=node_ds, i_ds=info_ds), code=JS_node_select)
    node_ds.selected.js_on_change('indices', callback_node)

    # Go through and process the idp links information
    inter_dom_info = {"link": [], "node": {},
        "edge_default": {"color": "#cccccc", "sel_color": bkp.Spectral4[2]},
        "node_default": {"color": bkp.Spectral4[0], "sel_color": bkp.Spectral4[2]}
    }
    domains = []
    if sw_ctrl_map is not None:
        with open(sw_ctrl_map, "r") as fin:
            tmp = json.loads(fin.read())
            for ctrl,ctrl_d in tmp["ctrl"].items():
                # Process the switches of the domain
                for sw in ctrl_d["sw"]:
                    if ctrl not in domains:
                        domains.append(ctrl)
                    i = domains.index(ctrl)
                    if sw in assoc_data:
                        sw = assoc_data[sw]
                    inter_dom_info["node"][sw] = bkp.Pastel1[9][i]

                # Process the inter-domain links
                for dom,dom_d in ctrl_d["dom"].items():
                    for info in dom_d:
                        info_from = info["sw"]
                        info_to = info["sw_to"]
                        if info_from in assoc_data:
                            info_from = assoc_data[info_from]
                        if info_to in assoc_data:
                            info_to = assoc_data[info_to]

                        idp = "%s-%s" % (info_from, info_to)
                        if idp not in inter_dom_info["link"]:
                            inter_dom_info["link"].append(idp)

    # Create container to show info of clicked nodes and connected links
    embed_div = bkm.Div(text="<div id='embed'>Click on node to view edge info!</div>",
        style={"padding": "20px", "width": "80%", "background": "#eee", "box-sizing":
                "border-box"})

    # Create dd to show or hide multi-controller domains
    dom_dd = bkm.Select(title="Show Domains:", value="No", options=["No", "Yes"], height=50,
                            sizing_mode="stretch_width")
    dom_dd.js_on_change("value", bkm.CustomJS(args=dict(n_ds=node_ds, e_ds=edge_ds,
                            id_info=inter_dom_info), code=JS_select_display_dom))

    # Create dd to change port indicator field
    info_ren = get_plot_port_ind(plot)
    type_dd = bkm.Select(title="Show Used For:", value="Primary", options=["Primary", "Secondary",
                                "Splice", "Total"], height=50, sizing_mode="stretch_width")
    type_dd.js_on_change("value", bkm.CustomJS(args=dict(ui=info_ren, plot=plot),
                            code=JS_select_path_type))

    # Configure the output file attributes, built the page layout and show the graph
    ui_ctrls = gen_plot_style_controls(plot)
    if sw_ctrl_map is not None:
        ui_ctrls.append(dom_dd)
    ui_ctrls.append(type_dd)

    opt_layout = bkl.column(children=ui_ctrls, width=120)
    main_layout = bkl.row(children=[opt_layout, bkl.Spacer(width=10), plot])
    page_layout = bkl.column(children=[main_layout, embed_div],
                                sizing_mode="scale_both", margin=(5,20,5,5))
    return page_layout

if __name__ == "__main__":
    # Initiate the argument parser and validate arguments
    parser = ArgumentParser("Helix Path Graph")
    parser.add_argument("--topo", required=True, type=str,
            help="Topology file (.dot or .gml)")
    parser.add_argument("--data", required=True, type=str,
            help="YATES helix iterface path JSON file")
    parser.add_argument("--node_assoc", required=False, type=str, default=None,
            help="Node association JSON file (optional)")
    parser.add_argument("--sw_ctrl_map", required=False, type=str, default=None,
            help="HelixMC switch to controller JSON map file (optional)")
    parser.add_argument("--host_name", required=False, type=str, default=None,
            help="JSON File that specifies host names for nodes (optional)")
    args = parser.parse_args()

    if not os.path.exists(args.topo) or not os.path.isfile(args.topo):
        print("Please enter a valid topology file")
        exit(1)

    if not os.path.exists(args.data) or not os.path.isfile(args.data):
        print("Please enter a valid YATES Helix interface path JSON file")
        exit(1)

    # If the node assoc file is invalid do not load it
    if (args.node_assoc is None or not os.path.exists(args.node_assoc)
            or not os.path.isfile(args.node_assoc)):
        args.node_assoc = None

    # If the switch to controller map argument is invalid do not load it
    if (args.sw_ctrl_map is None or not os.path.exists(args.sw_ctrl_map)
            or not os.path.isfile(args.sw_ctrl_map)):
        args.sw_ctrl_map = None

    # If the host name file argument is invalid do not load it
    if (args.host_name is None or not os.path.exists(args.host_name)
            or not os.path.isfile(args.host_name)):
        args.host_name = None

    output_file("helix_path_graph.html", title="Helix Path Graph")
    show(generate_page(args.topo, args.data, args.node_assoc, args.sw_ctrl_map, args.host_name))
