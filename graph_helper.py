#!/user/bin/python
import math

import geopy.distance as geodist
import pygraphviz
import networkx as nx

from bokeh import models as bkm
from bokeh.models import graphs as bkg
from bokeh import layouts as bkl
from bokeh import transform as bkt
from bokeh import palettes as bkp


# ----------------------------------------------------------------------------------
# Helper script that contains shared helper methods to generate topology graphs
# ----------------------------------------------------------------------------------

def __load_topo_gml(fpath):
    """ Load a .gml file topology """
    try:
        return nx.read_gml(fpath, label="id")
    except Exception:
        return None

def __load_topo_dot(fpath):
    """ Load a .dot file topology """
    try:
        return nx.drawing.nx_agraph.read_dot(path=fpath)
    except Exception:
        return None

def guess_topo_format(fpath):
    """ Guess the format of a topology file `fpath` by first trying to load it as a
    '.dot' file and then a '.gml' file.

    Args:
        fpath (str): Path to the file we want to load

    Returns:
        networkx.Graph: Loaded graph object or None if invalid file
    """
    try_load = []

    if fpath.endswith(".gml"):
        try_load = [__load_topo_gml, __load_topo_dot]
    else:
        try_load = [__load_topo_dot, __load_topo_gml]

    g = None
    for func in try_load:
        g = func(fpath)
        if g is not None:
            break
    return g

def __clean_graph(g, host_names):
    """ Clean a graph object by adding sensible values to nodes if there are missing attributes.
    Remove any nodes from the graph that should not be displayed (is internal or type == "host").

    Node Required Fields:
        label -> use node name if not defined
        id -> use node name if not defined
        Latitude -> If no coordinates, use spring layout to position (set to x position)
        Longitude -> If no coordinates, use spring layout to position (set to y position)
        Country -> set to 'N/A' if not defined
        Host -> If `host_names` is None, auto generate by appending "H" to the ID. If not None,
            use `host_names` which maps a label name to a string. If node label not in `host_name`
            (and `host_names` not null) set host to empty string.

    Note:
        Currently, all nodes are auto-positioned if any node contains no "Longitude" or "Latitude"
        attribute (i.e. no coordinates).

    Args:
        g (networkx.Graph): Graph object to clean
        host_names (dict): Dictionary that maps a host label to a host name or None

    Returns:
        networkx.Graph: Cleaned up graph object ready to display
    """
    found_no_coordinate = False
    remove = []
    for n,nd in g.nodes(data=True):
        # Add label attribute as the node name if one dosen't exist
        if "label" not in nd:
            nd["label"] = n

        # Add a ID field if one dosen't exist
        if "id" not in nd:
            nd["id"] = n

        # Only show internal nodes
        if "Internal" in nd and nd["Internal"] == 0:
            print("Removing non internal node %s (ID: %s)" % (nd["label"], n))
            remove.append(n)
            continue

        # Remove any host nodes (if they exist)
        if "type" in nd and nd["type"] == "host":
            print("Removing host node %s (ID: %s)" % (nd["label"], n))
            remove.append(n)
            continue

        # If the node has no coordinates can't show it
        if "Latitude" not in nd or "Longitude" not in nd:
            print("Coordinates do not exist for node %s" % n)
            found_no_coordinate = True

        # Add country if field dosen't exist
        if "Country" not in nd:
            nd["Country"] = "N/A"

        # Initiate the host name
        if host_names is not None:
            # Use the host name dictionary
            if nd["label"] in host_names:
                nd["Host"] = host_names[nd["label"]]
            else:
                nd["Host"] = ""
        else:
            # Automatically generate from ID
            nd["Host"] = "h%s" % nd["id"]

    # Remove any invalid notes
    for rem in remove:
        g.remove_node(rem)

    # If we have some nodes without coordinates add a default coordinate using a layout
    # to all nodes
    if found_no_coordinate:
        pos = nx.spring_layout(g)
        for n,cord in pos.items():
            g.nodes[n]["Longitude"] = cord[0]
            g.nodes[n]["Latitude"] = cord[1]

    return g

def distance(a_lat, a_lon, b_lat, b_lon):
    """ Compute the distance in km between two set of coordinates.
    Method expects coordinates in latitude, longitude order (y,x)

    Args:
        a_lat (float): Latitude of first coordinate
        a_lon (float): Longitude of first coordinate
        b_lat (float): Latitude of second coordinate
        b_lon (float): Longitude of second coordinate

    Returns:
        float: Distance between two points in KM
    """
    c1 = (a_lat, a_lon)
    c2 = (b_lat, b_lon)
    return geodist.vincenty(c1, c2).km

def area_dim(nodes, labels):
    """ Compute the width and height in km for the square that encasulates all
    points in the topology using the coordinate dictionary `nodes`. Node
    coordinates are tuples in format (longitude, latitude).

    Args:
        nodes (dict of tuple): Dictionary of node coordinates
        labels (dict of str): Dictionary that contains node labels

    Returns:
        (float, float, float, float, float, float): width and height in km
        followed by the min lat, max lat, min lon and max lon values.
    """
    # Lat constraint is -90 to 90
    # Lon constraint is -180 to 180
    min_lat = 100
    max_lat = -100
    min_lon = 200
    max_lon = -200

    min_lat_lbl = ""
    max_lat_lbl = ""
    min_lon_lbl = ""
    max_lat_lbl = ""

    # Go through all nodes and find the min max of lat and lon
    for n,c in nodes.items():
        if c is None:
            continue
        lon = c[0]
        lat = c[1]
        if lon < min_lon:
            min_lon = lon
            min_lon_lbl = labels[n]
        if lon > max_lon:
            max_lon = lon
            max_lon_lbl = labels[n]

        if lat < min_lat:
            min_lat = lat
            min_lat_lbl = labels[n]
        if lat > max_lat:
            max_lat = lat
            max_lat_lbl = labels[n]

    # Compute the size of the rect and return results
    width = distance(0, min_lon, 0, max_lon)
    height = distance(min_lat, 0, max_lat, 0)
    #return (width, height, min_lon_lbl, max_lon_lbl, min_lat_lbl, max_lat_lbl)
    return (width, height, min_lon, max_lon, min_lat, max_lat)


# JS callback that toogles the visibility of a UI element
JS_vis_toogle = """var sel = cb_obj.value;
if (sel == "Yes") {
    ui.visible = true;
} else {
    ui.visible = false;
}
"""


# JS callback that changes the display field for a UI element (labels)
JS_vis_change_text_field = """var sel = cb_obj.value;
ui.attributes.text.field = change_val[sel];
ui.change.emit();
"""


# JS callback that modifies a glyph attribute
JS_glyph_attr_mod = """var value = cb_obj.value;
ui.glyph[attr] = value;

if ((ui.hover_glyph != null) && (typeof ui.hover_glyph == "object") &&
            (attr in ui.hover_glyph))
    ui.hover_glyph[attr] = value;
if ((ui.selection_glyph != null) && (typeof ui.selection_glyph == "object") &&
            (attr in ui.selection_glyph))
    ui.selection_glyph[attr] = value;
if ((ui.nonselection_glyph != null) &&
            (typeof ui.nonselection_glyph == "object") &&
            (attr in ui.nonselection_glyph))
    ui.nonselection_glyph[attr] = value;

// Emit a x_range plot to fix issue where nodes and lines disappear on scale
plot.x_range.change.emit();
"""


# ------------------------------------------------------------------------------


def init_nx(topo_file, edge_ds_dict, node_extra=None, edge_init=None, host_names=None):
    """ Initiate a networkx topology object and edge information DS object which
    will beused to display information on a topology graph. The topology is loaded
    from `topo_file` while `edge_ds_dict` contains the keys and attributes used for
    the returned edge information DS. Nodes of the topology are cleaned by calling
    ``__clean_graph`` and augmneted with extra attributes defined by `node_extra`.
    Edges of the topology are added to `edge_ds_dict` (converted to returned DS
    column data store). Extra attributes are added to the edge by calling `edge_init`
    on every edge. Every edge will contain position of indicator lines which show
    information on both ends of a link. The start coordinate of the indicator line
    are the coordinates of the link end while the end coordinates are the quarter
    points closest to the link end (midpoint of midpoints of edge line). The
    indicator shows info for the node it's closest to, i.e. if we have a link between
    A and B, the indicator closest to A will show traffic on the port leading to B.

    Args:
        topo_file (str): Path of topology file to load
        edge_ds_dict (dict): Edge dictionary used to generate the final DS object
            with all required key attributes.
        node_extra (dict): Values to augment to nodes loaded from topology.
            Defaults to None, do not add any extra attributes.
        edge_init (lambda d : f): Method called to add extra attributes to a
            edge. Defaults to None, no extra attributes.
        host_names (dict): List of host names for each node (prevent auto
            generating names).

    Returns:
        (nx.Graph, dict, bkm.ColumnDataSource): NetworkX topology, dictionary
            with node positions in format (lon/x, lat/y) and edge information
            column data-source object to be used to display port info on topo
            graph.
    """
    # Read GML file and augment the data with extra node info
    pos = {}
    g = guess_topo_format(topo_file)
    g = __clean_graph(g, host_names)

    for n,nd in g.nodes(data=True):
        # Only show internal nodes
        if "Internal" in nd and nd["Internal"] == 0:
            print("Removing non internal node %s (ID: %d)" % (nd["label"], n))
            g.remove_node(n)
            continue

        # If the node has no coordinates can't show it
        if "Latitude" not in nd or "Longitude" not in nd:
            print("Coordinates do not exist for node %s" % n)
            continue

        # Extract the position and add the host name if file loaded
        pos[n] = (float(nd["Longitude"]), float(nd["Latitude"]))

        # Augment extra data to node
        if node_extra is not None:
            for mn_k, mn_v in node_extra.items():
                nd[mn_k] = mn_v

    # Array that holds the edges we have already processed
    proc_edge = []

    # Iterate through all edges of the graph
    for src,dst in g.edges():
        # Ignore any nodes that do not have position values
        if src in pos and dst in pos and pos[src] is not None and pos[dst] is not None:
            x1 = pos[src][0]
            y1 = pos[src][1]
            x2 = pos[dst][0]
            y2 = pos[dst][1]

            # Compute the distance between the two nodes in km
            #dist = distance(pos[src][1], pos[src][0], pos[dst][1], pos[dst][0])
            #print("Distance from %s to %s is %d km" % (src, dst, dist))

            # Add a default speed if one dosen't exist
            #if "Speed" not in g.edges[(src, dst)]:
            #    g.edges[(src, dst)]["Speed"] = "1Gbps"

            # Compute midpoint between the two nodes
            mid_x = (x1 + x2) / 2
            mid_y = (y1 + y2) / 2

            # Add the bi-direction link info the utilisation data dictionary
            if (src, dst) not in proc_edge:
                edge_ds_dict["from"].append(src)
                edge_ds_dict["to"].append(dst)
                proc_edge.append((src, dst))

                # Trigger the custom priming method if set
                if (edge_init is not None):
                    edge_init(edge_ds_dict)

                # Work out the indicator end point and add X to line
                end_x = (x1 + mid_x) / 2
                end_y = (y1 + mid_y) / 2
                edge_ds_dict["xs"].append([x1, end_x])
                edge_ds_dict["ys"].append([y1, end_y])

            if (dst, src) not in proc_edge:
                edge_ds_dict["from"].append(dst)
                edge_ds_dict["to"].append(src)
                proc_edge.append((dst, src))

                # Trigger the custom priming method if set
                if (edge_init is not None):
                    edge_init(edge_ds_dict)

                # Work out the indicator end point and add X to line
                end_x = (mid_x + x2) / 2
                end_y = (mid_y + y2) / 2
                edge_ds_dict["xs"].append([x2, end_x])
                edge_ds_dict["ys"].append([y2, end_y])

    # Retrieve the networkx graph, position dictionary and DS
    return g, pos, bkm.ColumnDataSource(edge_ds_dict)

def init_plot (title, g, pos, tooltip=[("index", "$index"), ("Label", "@label"),
                ("Host", "@Host")], port_ind_ds=None, port_ind_field=None,
                port_ind_scale=None, port_ind_title=None):
    """ Intiate a new plot showing information on a networkx graph topology.
    Method will configure the plot tools and network renderer as well as other
    componenets required, such as port indicators.

    Note:
        A `node_ds` callback needs to be added to trigger an action on node
        select (pressing a node).

    Args:
        title (str): Title of the plot
        g (nx.Graph): Networkx topology to show on plot
        pos (dict): Position dictionary to use to draw topo nodes
        tooltip (list of tup): Optional tooltip items to display on hover of plot nodes.
        port_ind_ds (bkm.ColumnDataSource): Port indicator datasource for values and position
        port_ind_field (str): Field to use for port indicator value
        port_ind_scale (tuple): List of port indicator scale in format (low, high).
        port_ind_title (str): Title to show on the port indicator bar

    Return:
        (bkm.Plot, bkm.Render): Initiated plot and graph renderer
    """
    pdim = area_dim(pos, nx.get_node_attributes(g, "label"))
    print("NODE AREA:\n  WIDTH:%dkm, HEIGHT:%dkm\n  (%s|%s) (%s|%s)" % pdim)
    print("-----------------------")

    # Min max longitude
    x1 = pdim[2]
    x2 = pdim[3]
    # Min max latitude
    y1 = pdim[4]
    y2 = pdim[5]

    # Add 2% padding
    x1 -= (abs(x1) * 0.04)
    x2 += (abs(x2) * 0.04)
    y1 -= (abs(y1) * 0.04)
    y2 += (abs(y2) * 0.04)

    init_w = abs(x1) + abs(x2)
    init_h = abs(y1) + abs(y2)

    if (init_w > init_h):
        # Height is larger so adjust range to keep spect ratio
        new_w = (90/360) * init_h
        diff_w = init_w - new_w
        x1 -= (new_w / 2)
        x2 += (new_w / 2)
    else:
        # Width is larger so adjust range to keep aspect ratio
        new_h = (360/90) * init_w
        diff_h = init_h - new_h
        y1 -= (new_h / 2)
        y2 += (new_h / 2)

    xr = bkm.Range1d(x1, x2)
    yr = bkm.Range1d(y1, y2)

    # XXX: It's important we pay attention to the range size ratio otherwise the nodes
    # will looked squashed!
    plot = bkm.Plot(plot_width=1600, plot_height=800, x_range=xr, y_range=yr,
                    sizing_mode="scale_both")
    plot.title.text = title

    # Initiate a renderer from the networkx data and set the node and edge style
    graph_ren = bkg.from_networkx(g, pos, scale=1, center=(0,0))
    graph_ren.name = "TopoRenderer"
    node_ren = graph_ren.node_renderer
    edge_ren = graph_ren.edge_renderer
    node_ds = node_ren.data_source
    edge_ds = edge_ren.data_source

    # Add colors values to the node and edges
    node_ds.data["color"] = ([bkp.Spectral4[0]] * len(node_ds.data["id"]))
    node_ds.data["sel_color"] = ([bkp.Spectral4[2]] * len(node_ds.data["id"]))
    edge_ds.data["color"] = (["#cccccc"] * len(edge_ds.data["start"]))
    edge_ds.data["sel_color"] = ([bkp.Spectral4[2]] * len(edge_ds.data["start"]))
    edge_ds.data["speed"] = ([""] * len(edge_ds.data["start"]))
    edge_ds.data["midX"] = ([0] * len(edge_ds.data["start"]))
    edge_ds.data["midY"] = ([0] * len(edge_ds.data["start"]))

    # Compute the edge midpoint value (for labels) and add speed information
    for i in range(len(edge_ds.data["start"])):
        start = edge_ds.data["start"][i]
        end = edge_ds.data["end"][i]
        speed = "?Gbps"
        if "capacity" in edge_ds.data:
            speed = edge_ds.data["capacity"][i]

        x1 = pos[start][0]
        y1 = pos[start][1]
        x2 = pos[end][0]
        y2 = pos[end][1]
        mid_x = (x1 + x2) / 2
        mid_y = (y1 + y2) / 2

        edge_ds.data["speed"][i] = speed
        edge_ds.data["midX"][i] = mid_x
        edge_ds.data["midY"][i] = mid_y

    # Define node and edge glybs to draw on graph
    node_ren.glyph = bkm.Circle(name="NodeGlyph", size=10, fill_color="color")
    node_ren.selection_glyph = bkm.Circle(name="NodeSelectGlyph", size=10, fill_color="sel_color")
    node_ren.hover_glyph = bkm.Circle(name="NodeHoverGlyph", size=10, fill_color=bkp.Spectral4[1])
    node_ren.nonselection_glyph = bkm.Circle(name="NodeNonselectGlyph", size=10, fill_color="color")
    edge_ren.glyph = bkm.MultiLine(name="EdgeGlyph", line_color="color", line_width=1)
    edge_ren.selection_glyph = bkm.MultiLine(name="EdgeSelectGlyph", line_color="sel_color")
    edge_ren.nonselection_glyph = bkm.MultiLine(name="EdgeNonselectGlyph", line_color="color", line_width=1)

    # Only nodes can be hovered over while nodes and linked edges are show selectable
    graph_ren.selection_policy = bkg.NodesAndLinkedEdges()
    graph_ren.inspection_policy = bkg.NodesOnly()


    # -------------------- GRAPH TOOLS ----------------------


    # Initiate the tools for interaction with the polot, ensure scrolling is active
    zoom_tl = bkm.WheelZoomTool()
    hover_tl = bkm.HoverTool(show_arrow=False, tooltips=tooltip)
    plot.add_tools(hover_tl)

    # Add the tools to the toolbar and esnure zoom is active
    plot.add_tools(hover_tl, bkm.TapTool(), zoom_tl, bkm.PanTool(), bkm.ResetTool(), bkm.SaveTool())
    plot.toolbar.active_scroll = zoom_tl
    plot.renderers.append(graph_ren)


    # -------------------- GRAPH LABELS AND USAGE INDICATOR ---------------------


    # Add a set of labels that annote the nodes and edges on graph
    node_labels = bkm.LabelSet(x="Longitude", y="Latitude", text="Host", x_offset=5, y_offset=5,
                                source=node_ds, render_mode="canvas", level="overlay",
                                background_fill_color="#ffffff", text_font_size="10pt",
                                name="NodeLabels")
    plot.add_layout(node_labels)

    edge_labels = bkm.LabelSet(x="midX", y="midY", text="speed", x_offset=0, y_offset=0,
                            source=edge_ds, render_mode="canvas", level="overlay",
                            text_color="#ff0000", background_fill_alpha=0,
                            text_font_size="8pt", background_fill_color="#ffffff",
                            visible=False, name="EdgeLabels")
    plot.add_layout(edge_labels)

    # Add a label the specifies the distance the network covers
    #size_txt = "Width: %dKM | Height: %dKM" % (pdim[0], pdim[1])
    #size_lbl = bkm.Label(x=5, y=5, x_units='screen', y_units='screen', text=size_txt,
    #        render_mode="css", background_fill_color='white', background_fill_alpha=0.6,
    #        name="NetSizeLbl")
    #plot.add_layout(size_lbl)

    # Initiate the link port indicator (if provided)
    if (port_ind_ds is not None and port_ind_field is not None and
                    port_ind_scale is not None and port_ind_title is not None):
        port_ren = bkm.GlyphRenderer(name="PortIndicator", data_source=port_ind_ds)
        mapper = bkt.linear_cmap(field_name=port_ind_field, palette=bkp.Viridis256, low=port_ind_scale[0],
                                    high=port_ind_scale[1])
        port_ren.glyph = bkm.MultiLine(xs="xs", ys="ys", line_color=mapper, line_width=4)
        plot.add_layout(port_ren)

        # Add a color bar to outline scale of link usage indicators
        color_bar = bkm.ColorBar(name="PortIndicatorScale", color_mapper=mapper["transform"],
                                    location=(0,0), title=port_ind_title, title_standoff=20)
        plot.add_layout(color_bar, 'right')

    # Return the initiated plot and the main network renderer
    return (plot, graph_ren)

def gen_plot_style_controls(plot):
    """ Initiate and return an array of plot style controls to be added to the a layout. The
    style controls modify indicator widths as well as provide drop downs to show and hide elements
    shown on the plot.

    Returns:
        list of obj: List of controls to add to a layout element. The first element is a spacer.
    """
    res = [bkl.Spacer(height=24)]

    # Usage indicator line width control
    port_ind = get_plot_port_ind(plot)
    if port_ind is not None:
        usage_width_sl = bkm.Slider(start=2, end=20, step=1, value=port_ind.glyph.line_width,
                                        title="Indicator Width")
        usage_width_sl.js_on_change("value", bkm.CustomJS(args=dict(ui=port_ind, attr="line_width",
                                        plot=plot), code=JS_glyph_attr_mod))
        res.append(usage_width_sl)

    # Node label visibility control
    node_labels = get_plot_node_labels(plot)
    if node_labels is not None:
        lbl_vis_dd = bkm.Select(title="Show Node Label", value="Yes", options=["Yes", "No"])
        lbl_vis_dd.js_on_change("value", bkm.CustomJS(args=dict(ui=node_labels), code=JS_vis_toogle))
        res.append(lbl_vis_dd)

    # Node label use text field selection control
    if node_labels is not None:
        lbl_vis_dd = bkm.Select(title="Label Field", value="Yes", options=["Host", "Label"])
        lbl_vis_dd.js_on_change("value", bkm.CustomJS(args=dict(ui=node_labels,
                                    change_val={"Host": "Host", "Label":"label"}),
                                    code=JS_vis_change_text_field))
        res.append(lbl_vis_dd)

    # Edge capacity visilibty control
    edge_labels = get_plot_edge_labels(plot)
    if edge_labels is not None:
        edge_lbl_vis_dd = bkm.Select(title="Show Edge Cap", value="No",
                                        options=["Yes", "No"])
        edge_lbl_vis_dd.js_on_change("value", bkm.CustomJS(args=dict(ui=edge_labels),
                                        code=JS_vis_toogle))
        res.append(edge_lbl_vis_dd)

    # Port indicator scale bar visibility control
    port_bar = get_plot_port_scale(plot)
    if port_bar is not None:
        ubar_vis_dd = bkm.Select(title="Show Usage Bar", value="Yes", options=["Yes", "No"])
        ubar_vis_dd.js_on_change("value", bkm.CustomJS(args=dict(ui=port_bar), code=JS_vis_toogle))
        res.append(ubar_vis_dd)

    # Node radius control
    net_ren = get_plot_network_renderer(plot)
    if net_ren is not None:
        node_ren = net_ren.node_renderer
        node_size_sl = bkm.Slider(start=10, end=20, step=1, value=node_ren.glyph.size, title="Node Size")
        node_size_sl.js_on_change("value", bkm.CustomJS(args=dict(ui=node_ren, attr="size", plot=plot),
                                        code=JS_glyph_attr_mod))
        res.append(node_size_sl)

    # Link width control
    edge_ren = net_ren.edge_renderer
    if edge_ren is not None:
        link_size_sl = bkm.Slider(start=1, end=10, step=1, value=edge_ren.glyph.line_width,
                                    title="Link Width")
        link_size_sl.js_on_change("value", bkm.CustomJS(args=dict(ui=edge_ren, attr="line_width",
                                        plot=plot), code=JS_glyph_attr_mod))
        res.append(link_size_sl)

    return res

def get_plot_node_labels(plot):
    """ Get the label set that annotates the nodes in a plot. The LabelSet object has to
    have the name 'NodeLabels' otherwise nothing is returned. Method assumes that the
    label set is added to the center layout element (no position specified).

    Args:
        plot (bkm.Plot): Plot to find and retrieve node labels from

    Returns:
        bkm.LabelSet: Set of labels if element found or null if nothing found
    """
    for el in plot.center:
        if isinstance(el, bkm.LabelSet) and el.name == "NodeLabels":
            return el
    return None

def get_plot_edge_labels(plot):
    """ Get the label set that annotates the edges in a plot. The LabelSet object has to
    have the name 'EdgeLabels' otherwise nothing is returned. Method assumes that the
    label set is added to the center layout element (no position specified).

    Args:
        plot (bkm.Plot): Plot to find and retrieve node labels from

    Returns:
        bkm.LabelSet: Set of labels if element found or null if nothing found
    """
    for el in plot.center:
        if isinstance(el, bkm.LabelSet) and el.name == "EdgeLabels":
            return el
    return None

def get_plot_port_scale(plot):
    """ Get the plot color bar scale indicator instance from the plot. The ColorBar object
    has to have the name 'ScaleColorBar' otherwise nothing is returned. Method will search
    through all possible layout placements for the color bar.

    Args:
        plot (bkm.Plot): Plot to find and retrieve color bar from

    Returns:
        bkm.ColorBar: Color bar object if found, otherwise null
    """
    for loc in [plot.right, plot.left, plot.above, plot.below, plot.center]:
        for el in loc:
            if isinstance(el, bkm.ColorBar) and el.name == "PortIndicatorScale":
                return el
    return None

def get_plot_port_ind(plot):
    """ Get the plot port indicator. The port indicator is a glyph renderer that has the
    name 'PortIndicator'. Method assumes that the render is added to the center layout
    element (no position specified for add layout).

    Args:
        plot (bkm.Plot): Plot to find and retrieve plot indicator

    Returns:
        bkm.GlyphRendere: Port indicator renderer or null if not found.
    """
    for el in plot.center:
        if isinstance(el, bkm.GlyphRenderer) and el.name == "PortIndicator":
            return el
    return None

def get_plot_network_renderer(plot):
    """ Get the plots network renderer that displays the nodes and links of the topology.
    Method assumes that the renderer is the renderer list and has the name "TopoRenderer".

    Args:
        plot (bkm.Plot): Plot to retrieve network renderer from

    Returns:
        bkm.GraphRendere: Network rendere from plot if found, otherwise null
    """
    for el in plot.renderers:
        if isinstance(el, bkm.GraphRenderer) and el.name == "TopoRenderer":
            return el
    return None

def order_insert(n, ls):
    """ Insert node `n` in list `ls` while mainting ascending order. Method checks where to
    insert element and returns the insertion index.

    Args:
        n (int): Node to insert
        ls (list): List to insert node into

    Returns:
        int: Index where the insertion occured or -1 if it was appended to end of list
    """
    insert_index = -1
    if len(ls) > 0:
        if ls[0] > n:
            insert_index = 0
        elif ls[-1] > n:
            for i in range(0, len(ls) - 1):
                if ls[i] > n:
                    insert_index = 0
                    break
                elif ls[i] < n and ls[i+1] > n:
                    insert_index = i+1
                    break

    # Should we append or insert at the index?
    if insert_index != -1:
        ls.insert(insert_index, n)
    else:
        ls.append(n)

    return insert_index
