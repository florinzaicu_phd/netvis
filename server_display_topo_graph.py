#!/user/bin/python
import os
from display_topo_graph import generate_page
from base_server import BaseServer


class Graph(BaseServer):
    TITLE = "Display Topology Graph"
    DATA_FILES = {"Default": [("topo.gml", "topo.dot")]}

    def load_data(self, name):
        data_dir = os.path.abspath(os.path.join("DATA", name))

        # Find which topology file type exists to load the correct one
        topo_input = os.path.join(data_dir, "topo.gml")
        if not os.path.exists(topo_input) or not os.path.isfile(topo_input):
            topo_input = os.path.join(data_dir, "topo.dot")

        # Check if we have an optional node association file
        node_assoc_input = os.path.join(data_dir, "node_assoc.json")
        if not os.path.exists(node_assoc_input) or not os.path.isfile(node_assoc_input):
            node_assoc_input = None

        # Check if we have an optional switch-to-controller mapping file
        sw_ctrl_map_input = os.path.join(data_dir, "sw_ctrl_map.json")
        if not os.path.exists(sw_ctrl_map_input) or not os.path.isfile(sw_ctrl_map_input):
            sw_ctrl_map_input = None

        # Check if we have an optional host name file
        host_name_input = os.path.join(data_dir, "host_name.json")
        if not os.path.exists(host_name_input) or not os.path.isfile(host_name_input):
            host_name_input = None

        layout = generate_page(topo_input, node_assoc_input, sw_ctrl_map_input,
                                host_name_input)
        self.update_page_content(layout)


# Generate the page
Graph()
