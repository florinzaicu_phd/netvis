#!/user/bin/python
import sys
import os
import re
from argparse import ArgumentParser

# Visualisation Libraries
from bokeh.io import show, output_file
from bokeh import models as bkm
from bokeh import layouts as bkl
from bokeh import palettes as bkp
from graph_helper import order_insert

# --------------------------------------------------------
# Graph that shows latency (path strech) CDF for all iterations
# --------------------------------------------------------


def generate_page(input):
    """ Generate and return the page layout showing the data from `input`. This page shows confidence
    distribution function (CDF) for average latency (as # nodes) as a percentage of total traffic in
    the network. To compute the total CDF for all iterations the cumulative % of traffic is first
    converted into a PDF (probability distribution function) similar to the latency stacked
    graph. Then the PDF for all values are averaged for all iterations and converted back to a
    cumulatie function (CDF).

    Returns:
        obj: Page layout to display
    """
    stats_data = {}
    max_iteration = 0
    min_latency = 9999
    max_latency = 0
    with open(input, "r") as fin:
        algo = ""
        iter = 0
        # Iterate through the lines in the data file
        for line in fin:
            line = line.strip()

            # Skip comment or empty lines
            if line == "" or line.startswith("#"):
                continue

            # If this is a result line process it
            elif ":" in line:
                node = int(line.split(":")[0].strip().strip("."))
                u = float(line.split(":")[1].strip())

                if node > max_latency:
                    max_latency = node
                if node < min_latency:
                    min_latency = node

                stats_data[algo]["raw"][iter][node] = u

            # Otherwise assume it's a algorithm iteration line
            else:
                reg = re.search("([a-zA-Z0-9_]+)\s([0-9]+)", line)
                algo = reg.group(1)
                iter = int(reg.group(2))

                # Prime the dict with default values, if new algo
                if algo not in stats_data:
                    stats_data[algo] = {
                        "raw": {},
                        "latency": [],
                        "percent": [],
                    }

                if (iter + 1) > max_iteration:
                    max_iteration = iter + 1
                stats_data[algo]["raw"][iter] = {}

    # Go through raw data and de-cumulate (compute PDF)
    for algo,algo_d in stats_data.items():
        for iter in sorted(algo_d["raw"].keys()):
            iter_d = algo_d["raw"][iter]
            nodes_sorted = sorted(iter_d.keys())
            for i in reversed(range(len(nodes_sorted))):
                n = nodes_sorted[i]
                p = iter_d[n]
                if i > 0:
                    iter_d[n] -= iter_d[nodes_sorted[i-1]]

    # Iterate through the raw data and compute the average PDF over the iteration population
    algo_ls = []
    for algo,algo_d in stats_data.items():
        algo_ls.append(algo)

        for iter in sorted(algo_d["raw"].keys()):
            iter_d = algo_d["raw"][iter]
            for n in sorted(iter_d.keys(), reverse=True):
                p = iter_d[n]

                if n not in algo_d["latency"]:

                    # Insert the latency data in node order
                    insert_index = order_insert(n, algo_d["latency"])
                    if insert_index != -1:
                        algo_d["percent"].insert(insert_index, p)
                    else:
                        algo_d["percent"].append(p)

                else:
                    index = algo_d["latency"].index(n)
                    algo_d["percent"][index] += p

        for i in range(len(algo_d["percent"])):
            node = algo_d["latency"][i]
            algo_d["percent"][i] = (algo_d["percent"][i] / max_iteration)

        del algo_d["raw"]

    # Go through the data and compute the CDF
    for algo,algo_d in stats_data.items():
        for i in range(len(algo_d["percent"])):
            if i > 0:
                algo_d["percent"][i] += algo_d["percent"][i-1]

    # Initiate the plot and axis
    plot = bkm.Plot(toolbar_location="right", sizing_mode="stretch_both")
    plot.title.text = "Average Latency / Path Strech CDF"
    x_axis = bkm.LinearAxis(bounds=(min_latency, max_latency), axis_label="Latency (# Nodes)",
                ticker=bkm.SingleIntervalTicker(interval=1, num_minor_ticks=0))
    plot.add_layout(x_axis, 'below')
    y_axis = bkm.LinearAxis(bounds=(0, 1), axis_label="F(x)")
    plot.add_layout(y_axis, 'left')
    plot.add_layout(bkm.Grid(dimension=0, ticker=x_axis.ticker))
    plot.add_layout(bkm.Grid(dimension=1, ticker=y_axis.ticker))

    legend_items = []
    colors = bkp.Category10[10]
    colors.extend(bkp.Pastel1[8])
    for i in range(len(algo_ls)):
        algo = algo_ls[i]
        algo_d = stats_data[algo]
        ds_dict = {
                "x": algo_d["latency"],
                "y": algo_d["percent"],
                "name": ([algo] * len(algo_d["percent"]))
        }
        ds = bkm.ColumnDataSource(ds_dict)
        glyph = bkm.Line(x="x", y="y", line_width=2, line_color=colors[i])
        ren = bkm.GlyphRenderer(name=algo, data_source=ds, glyph=glyph, visible=True)
        plot.renderers.append(ren)

        glyph_pt = bkm.Cross(x="x", y="y", line_width=6, line_color=colors[i])
        ren_pt = bkm.GlyphRenderer(name="%s_pt" % algo, data_source=ds, glyph=glyph_pt, visible=True)
        plot.renderers.append(ren_pt)

        legend_item = bkm.LegendItem(label=algo, renderers=[ren, ren_pt])
        legend_items.append(legend_item)

    # Create the legend and add it under the plot
    legend = bkm.Legend(items=legend_items, location="center", orientation="horizontal",
                        background_fill_alpha=1.0, click_policy="hide")
    plot.add_layout(legend, "below")

    # Add a hover tuil and build the toolbar
    hover_tl = bkm.HoverTool(show_arrow=False, tooltips=[("Latency", "@x"), ("F(x)", "@y"),
                                ("Algorithm", "@name")])
    zoom_tl = bkm.WheelZoomTool()
    plot.toolbar.active_scroll = zoom_tl
    plot.add_tools(hover_tl, bkm.TapTool(), zoom_tl, bkm.PanTool(), bkm.ResetTool(), bkm.SaveTool())

    # Configure the output file attributes, built the page layout and show the graph
    page_layout = bkl.column(children=[plot], sizing_mode="scale_both", margin=(5,20,5,5))
    return page_layout

if __name__ == "__main__":
    # Initiate the argument parser and validate arguments
    parser = ArgumentParser("Latency CDF Graph")
    parser.add_argument("--data", required=True, type=str, help="YATES latency file")
    args = parser.parse_args()

    if not os.path.exists(args.data) or not os.path.isfile(args.data):
        print("Please enter a valid latency file")
        exit(1)

    output_file("latency_cdf_graph.html", title="Latency CDF Graph")
    show(generate_page(args.data))
