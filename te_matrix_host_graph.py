#!/user/bin/python
import sys
import os
import re
import math
from argparse import ArgumentParser

# Visualisation Libraries
from bokeh.io import show, output_file
from bokeh import models as bkm
from bokeh import layouts as bkl
from bokeh import palettes as bkp
from bokeh.plotting import figure


# --------------------------------------------------------
# Graph that shows TE matrix host traffic statitcs as bar graphs
# --------------------------------------------------------


JS_SCALE_CHANGE = """var scale_txt = cb_obj.value;
var scale = parseFloat(scale_txt);
if (isNaN(scale)) {
    alert("Please enter a valid numeric scale factor!");
    return;
}
if (scale < 0.1) {
    alert("Scale factor can't be less than 0.1");
    return;
}

// Compute the scale modification required based on the old scale value
var scale_mod = scale / opt_ds.data["scale"][0];
opt_ds.data["scale"][0] = scale;

// Update the data to match the new scale
for (var i = 0; i < send_ds.data["top"].length; i++) {
    var old_send = send_ds.data["top"][i];
    send_ds.data["top"][i] = old_send * scale_mod;

    var old_recv = recv_ds.data["top"][i];
    recv_ds.data["top"][i] = old_recv * scale_mod;
}

// Change the title and empit DS changes
var old_title = plot.title.text.split("(")[0];
plot.title.text = old_title + "(Scale: " + scale + ")";
send_ds.change.emit();
recv_ds.change.emit();
"""

JS_CHANGE_UNITS = """var units_txt = cb_obj.value;
var units = 1;
if (units_txt == "kbps")
    units = 1000;
else if (units_txt == "mbps")
    units = 1000 * 1000;
else if (units_txt == "gbps")
    units = 1000 * 1000 * 1000;

// Get the old unit text to work out required DS mod
var old_units_txt = opt_ds.data["units"][0];
var old_units = 1;
if (old_units_txt == "kbps")
    old_units = 1000;
else if (old_units_txt == "mbps")
    old_units = 1000 * 1000;
else if (old_units_txt == "gbps")
    old_units = 1000 * 1000 * 1000;

// Compute unit change required DS scaling
var units_mod = units / old_units;
opt_ds.data["units"][0] = units_txt;

// Update the datasource to show new units
for (var i = 0; i < send_ds.data["units"].length; i++) {
    var old_send = send_ds.data["top"][i];
    send_ds.data["top"][i] = old_send / units_mod;

    var old_recv = recv_ds.data["top"][i];
    recv_ds.data["top"][i] = old_recv / units_mod;

    send_ds.data["units"][i] = units_txt;
    recv_ds.data["units"][i] = units_txt;
}

// Update title and emit DS changes
plot.left[0].axis_label = "Traffic (" + units_txt + ")";
send_ds.change.emit();
recv_ds.change.emit();
"""

JS_TYPE_CHANGE = """var type = cb_obj.value;
// Process the display units
var units = 1;
if (opt_ds.data["units"][0] == "kbps")
    units = 1000;
else if (opt_ds.data["units"][0] == "mbps")
    units = 1000 * 1000;
else if (opt_ds.data["units"][0] == "gbps")
    units = 1000 * 1000 * 1000;

// Retrieve the scale factor
var scale = opt_ds.data["scale"][0];

// Process target type
var target_key = "";
if (type == "Iteration") {
    target_key = iter_sl.value - 1;
    plot.title.text = "TE Matrix Host Traffic - Iteration " + iter_sl.value;
    iter_sl.visible = true;
} else if (type == "Total") {
    target_key = "total";
    plot.title.text = "TE Matrix Host Traffic - Total";
    iter_sl.visible = false;
} else if (type == "Average") {
    target_key = "avg";
    plot.title.text = "TE Matrix Host Traffic - Average";
    iter_sl.visible = false;
}

// Add scale factor to title
plot.title.text += " (Scale: " + scale + ")";

// Update data based on selection
var send_target = stats_data[target_key]["send"];
var recv_target = stats_data[target_key]["recv"];
for (var i = 0; i < send_ds.data["top"].length; i++) {
    var value = (send_target[i] * scale) / units;
    send_ds.data["top"][i] = value;

    var value = (recv_target[i] * scale) / units;
    recv_ds.data["top"][i] = value;
}

// Emit DS changes
send_ds.change.emit();
recv_ds.change.emit();
"""

JS_ITER_CHANGE = """ var iter = cb_obj.value;
// Process the display units
var units = 1;
if (opt_ds.data["units"][0] == "kbps")
    units = 1000;
else if (opt_ds.data["units"][0] == "mbps")
    units = 1000 * 1000;
else if (opt_ds.data["units"][0] == "gbps")
    units = 1000 * 1000 * 1000;

// Retrieve the scale factor
var scale = opt_ds.data["scale"][0];

// Update DS to show selected iteration
var send_target = stats_data[iter - 1]["send"];
var recv_target = stats_data[iter - 1]["recv"];
for (var i = 0; i < send_ds.data["top"].length; i++) {
    var value = (send_target[i] * scale) / units;
    send_ds.data["top"][i] = value;

    var value = (recv_target[i] * scale) / units;
    recv_ds.data["top"][i] = value;
}

// Update title and emit changes
plot.title.text = "TE Matrix Host Traffic - Iteration " + iter + " (Scale: " + scale + ")";
send_ds.change.emit();
recv_ds.change.emit();
"""

def generate_page(te_matrix_input, host_input):
    """ Generate and return the page layout showing the data from `te_matrix_input`. TE
    matrix host traffic for both receiving and sending are shown as bar graphs for each
    host. Positive traffic represents reciving while negative sending. Hosts are provided
    by the host file `host_input`.

    Returns:
        obj: Page layout to display
    """
    stats_data = {}
    max_iteration = 0
    hosts = []

    # Read the hosts input and get the list of hosts
    with open(host_input, "r") as fin:
        for line in fin:
            line = line.strip()

            # Skip empty lines
            if line ==  "":
                continue

            hosts.append(line)

    # Prime the total and average part of the data array
    stats_data["total"] = {}
    stats_data["total"]["recv"] = [0.0] * len(hosts)
    stats_data["total"]["send"] = [0.0] * len(hosts)
    stats_data["avg"] = {}
    stats_data["avg"]["recv"] = [0.0] * len(hosts)
    stats_data["avg"]["send"] = [0.0] * len(hosts)

    # Rread the TE matrix file
    with open(te_matrix_input, "r") as fin:
        for line in fin:
            line = line.strip()

            # Skip empty lines
            if line == "":
                continue

            # Initiate the data array and process te matrix row columns
            stats_data[str(max_iteration)] = {}
            stats_data[str(max_iteration)]["recv"] = [0] * len(hosts)
            stats_data[str(max_iteration)]["send"] = [0] * len(hosts)

            tokens = line.split(" ")
            for i in range(len(tokens)):
                # Compute the from and to index based on the token index and skip if host send
                # to itself
                hfrom_index = math.floor(i / len(hosts))
                hto_index = i % len(hosts)
                if hfrom_index == hto_index:
                    continue

                val = float(tokens[i])

                # Set the stats
                stats_data[str(max_iteration)]["send"][hfrom_index] -= val
                stats_data[str(max_iteration)]["recv"][hto_index] += val

                # Set the aggregate stats
                stats_data["total"]["send"][hfrom_index] -= val
                stats_data["total"]["recv"][hto_index] += val

            max_iteration += 1

    # Compute average TE matrix traffic
    for i in range(len(hosts)):
        avg_send = stats_data["total"]["send"][i] / max_iteration
        avg_recv = stats_data["total"]["recv"][i] / max_iteration
        stats_data["avg"]["send"][i] = avg_send
        stats_data["avg"]["recv"][i] = avg_recv

    # Initiate the graph and data-sources
    colors = bkp.Category10[4]
    recv_ds = bkm.ColumnDataSource(dict(x=hosts, top=stats_data["0"]["recv"], units=(["bps"] * len(hosts))))
    send_ds = bkm.ColumnDataSource(dict(x=hosts, top=stats_data["0"]["send"], units=(["bps"] * len(hosts))))

    plot = figure(x_range=hosts, tools="", toolbar_location="right", sizing_mode="stretch_both")
    plot.vbar(x="x", top="top", width=1, source=recv_ds, line_color="black", fill_color=colors[2], legend="Receive")
    plot.vbar(x="x", top="top", width=1, source=send_ds, line_color="black", fill_color=colors[3], legend="Send")

    plot.xaxis.axis_label = "Hosts"
    plot.yaxis.axis_label = "Traffic (bps)"

    # Configure the title and legend location
    plot.title.text = "TE Matrix Host Traffic - Iteration 1 (Scale: 1)"
    plot.legend.location = "bottom_center"
    plot.legend.orientation = "horizontal"

    # Initiate the tools for the graph
    hover_tl = bkm.HoverTool(show_arrow=False, tooltips=[("Host", "@x"), ("Traffic", "@top"),
                                                            ("Unit", "@units")])
    zoom_tl = bkm.WheelZoomTool()
    plot.toolbar.active_scroll = zoom_tl
    plot.add_tools(hover_tl, bkm.TapTool(), zoom_tl, bkm.PanTool(), bkm.ResetTool(), bkm.SaveTool())

    # Datasource that stores options like scale and units
    opt_ds = bkm.ColumnDataSource(dict(scale=[1.0], units=["bps"]))

    # Initiate the controls

    # Scale text input
    scale_tx = bkm.TextInput(title="Scale Factor:", value="1.0")
    scale_change_callback = bkm.CustomJS(args=dict(opt_ds=opt_ds, recv_ds=recv_ds, send_ds=send_ds, plot=plot),
                                code=JS_SCALE_CHANGE)
    scale_tx.js_on_change("value", scale_change_callback)

    # Unit drop down
    unit_dd = bkm.Select(title="Units:", value="bps", options=["bps", "kbps", "mbps", "gbps"], height=50,
                        sizing_mode="stretch_width")
    unit_change_callback = bkm.CustomJS(args=dict(opt_ds=opt_ds, recv_ds=recv_ds, send_ds=send_ds, plot=plot),
                                            code=JS_CHANGE_UNITS)
    unit_dd.js_on_change("value", unit_change_callback)

    # Iteration slider
    iter_sl = bkm.Slider(start=1, end=max_iteration, step=1, value=1, title="Iteration",
                            height=50, sizing_mode="stretch_width")
    iter_change_callback = bkm.CustomJS(args=dict(opt_ds=opt_ds, stats_data=stats_data, recv_ds=recv_ds,
                                send_ds=send_ds, plot=plot), code=JS_ITER_CHANGE)
    iter_sl.js_on_change("value", iter_change_callback)

    # Type drop down
    type_dd = bkm.Select(title="Display:", value="Iteration", options=["Iteration", "Total", "Average",],
                            height=50, sizing_mode="stretch_width")
    type_change_callback = bkm.CustomJS(args=dict(opt_ds=opt_ds, stats_data=stats_data, recv_ds=recv_ds,
                                    send_ds=send_ds, plot=plot, iter_sl=iter_sl), code=JS_TYPE_CHANGE)
    type_dd.js_on_change("value", type_change_callback)


    # Configure the output file attributes, built the page layout and show the graph
    row_dd = bkl.row(children=[scale_tx, unit_dd, type_dd], sizing_mode="stretch_width")
    page_layout = bkl.column(children=[plot, row_dd, iter_sl], sizing_mode="scale_both",
                                margin=(5,20,5,5))
    return page_layout

if __name__ == "__main__":
    # Initiate the argument parser and validate arguments
    parser = ArgumentParser("TE Matrix Host Graph")
    parser.add_argument("--data", required=True, type=str, help="YATES TE matrix file")
    parser.add_argument("--host", required=True, type=str, help="YATES host file")
    args = parser.parse_args()

    if not os.path.exists(args.data) or not os.path.isfile(args.data):
        print("Please enter a valid TE Matrix file")
        exit(1)

    if not os.path.exists(args.host) or not os.path.isfile(args.host):
        print("Please enter a valid host mapping file")
        exit(1)

    output_file("te_matrix_host_graph.html", title="TE Matrix Host Graph")
    show(generate_page(args.data, args.host))
