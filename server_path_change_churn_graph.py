#!/user/bin/python
import os
from path_change_churn_graph import generate_page
from base_server import BaseServer


class Graph(BaseServer):
    TITLE = "Path Change Churn Graph"

    DATA_FILES = {
        "Default": ["TMChurnVsIterations.dat"],
    }

    def load_data(self, name):
        data_dir = os.path.abspath(os.path.join("DATA", name))
        input = os.path.join(data_dir, "TMChurnVsIterations.dat")
        layout = generate_page(input)
        self.update_page_content(layout)


# Generate the page
Graph()
